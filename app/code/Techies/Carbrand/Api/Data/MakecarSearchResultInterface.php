<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Api\Data;

/**
 * @api
 */
interface MakecarSearchResultInterface
{
    /**
     * Get Makes list.
     *
     * @return \Techies\Carbrand\Api\Data\MakecarInterface[]
     */
    public function getItems();

    /**
     * Set Makes list.
     *
     * @param \Techies\Carbrand\Api\Data\MakecarInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
