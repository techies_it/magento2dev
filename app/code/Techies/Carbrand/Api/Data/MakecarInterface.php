<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Api\Data;

/**
 * @api
 */
interface MakecarInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const MAKECAR_ID = 'makecar_id';

    /**
     * Make Car attribute constant
     * 
     * @var string
     */
    const MAKECAR = 'makecar';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getMakecarId();

    /**
     * Set ID
     *
     * @param int $makecarId
     * @return MakecarInterface
     */
    public function setMakecarId($makecarId);

    /**
     * Get Make Car
     *
     * @return mixed
     */
    public function getMakecar();

    /**
     * Set Make Car
     *
     * @param mixed $makecar
     * @return MakecarInterface
     */
    public function setMakecar($makecar);
}
