<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Api;

/**
 * @api
 */
interface MakecarRepositoryInterface
{
    /**
     * Save Make.
     *
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @return \Techies\Carbrand\Api\Data\MakecarInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Carbrand\Api\Data\MakecarInterface $makecar);

    /**
     * Retrieve Make
     *
     * @param int $makecarId
     * @return \Techies\Carbrand\Api\Data\MakecarInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($makecarId);

    /**
     * Retrieve Makes matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Carbrand\Api\Data\MakecarSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Make.
     *
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Carbrand\Api\Data\MakecarInterface $makecar);

    /**
     * Delete Make by ID.
     *
     * @param int $makecarId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($makecarId);
}
