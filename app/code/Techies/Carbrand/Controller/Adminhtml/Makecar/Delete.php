<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

class Delete extends \Techies\Carbrand\Controller\Adminhtml\Makecar
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('makecar_id');
        if ($id) {
            try {
                $this->makecarRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The Make has been deleted.'));
                $resultRedirect->setPath('techies_carbrand/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The Make no longer exists.'));
                return $resultRedirect->setPath('techies_carbrand/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('techies_carbrand/makecar/edit', ['makecar_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the Make'));
                return $resultRedirect->setPath('techies_carbrand/makecar/edit', ['makecar_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Make to delete.'));
        $resultRedirect->setPath('techies_carbrand/*/');
        return $resultRedirect;
    }
}
