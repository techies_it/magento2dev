<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

class Index extends \Techies\Carbrand\Controller\Adminhtml\Makecar
{
    /**
     * Makes list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Carbrand::makecar');
        $resultPage->getConfig()->getTitle()->prepend(__('Makes'));
        $resultPage->addBreadcrumb(__('Make'), __('Make'));
        $resultPage->addBreadcrumb(__('Makes'), __('Makes'));
        return $resultPage;
    }
}
