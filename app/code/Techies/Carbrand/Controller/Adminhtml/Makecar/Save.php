<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

class Save extends \Techies\Carbrand\Controller\Adminhtml\Makecar
{
    /**
     * Make factory
     * 
     * @var \Techies\Carbrand\Api\Data\MakecarInterfaceFactory
     */
    protected $makecarFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Techies\Carbrand\Model\UploaderPool
     */
    protected $uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Techies\Carbrand\Api\Data\MakecarInterfaceFactory $makecarFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Carbrand\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Techies\Carbrand\Api\Data\MakecarInterfaceFactory $makecarFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
    //    \Techies\Carbrand\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->makecarFactory      = $makecarFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper    = $dataObjectHelper;
      //  $this->uploaderPool        = $uploaderPool;
        $this->dataPersistor       = $dataPersistor;
        parent::__construct($context, $coreRegistry, $makecarRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Techies\Carbrand\Api\Data\MakecarInterface $makecar */
        $makecar = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['makecar_id']) ? $data['makecar_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $makecar = $this->makecarRepository->getById((int)$id);
            } else {
                unset($data['makecar_id']);
                $makecar = $this->makecarFactory->create();
            }
            $this->dataObjectHelper->populateWithArray($makecar, $data, \Techies\Carbrand\Api\Data\MakecarInterface::class);
            $this->makecarRepository->save($makecar);
            $this->messageManager->addSuccessMessage(__('You saved the Make'));
            $this->dataPersistor->clear('techies_carbrand_makecar');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('techies_carbrand/makecar/edit', ['makecar_id' => $makecar->getId()]);
            } else {
                $resultRedirect->setPath('techies_carbrand/makecar');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('techies_carbrand_makecar', $postData);
            $resultRedirect->setPath('techies_carbrand/makecar/edit', ['makecar_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Make'));
            $this->dataPersistor->set('techies_carbrand_makecar', $postData);
            $resultRedirect->setPath('techies_carbrand/makecar/edit', ['makecar_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Techies\Carbrand\Model\Uploader
     * @throws \Exception
     */
    protected function getUploader($type)
    {
        true;
        //return $this->uploaderPool->getUploader($type);
    }
}
