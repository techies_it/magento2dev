<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

class MassDelete extends \Techies\Carbrand\Controller\Adminhtml\Makecar\MassAction
{
    /**
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @return $this
     */
    protected function massAction(\Techies\Carbrand\Api\Data\MakecarInterface $makecar)
    {
        $this->makecarRepository->delete($makecar);
        return $this;
    }
}
