<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Make repository
     * 
     * @var \Techies\Carbrand\Api\MakecarRepositoryInterface
     */
    protected $makecarRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * Make collection factory
     * 
     * @var \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->makecarRepository = $makecarRepository;
        $this->filter            = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->successMessage    = $successMessage;
        $this->errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @return mixed
     */
    abstract protected function massAction(\Techies\Carbrand\Api\Data\MakecarInterface $makecar);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $makecar) {
                $this->massAction($makecar);
            }
            $this->messageManager->addSuccessMessage(__($this->successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('techies_carbrand/*/index');
        return $redirectResult;
    }
    
    
   
}
