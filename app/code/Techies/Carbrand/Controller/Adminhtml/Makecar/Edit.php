<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

class Edit extends \Techies\Carbrand\Controller\Adminhtml\Makecar
{
    /**
     * Initialize current Make and set it in the registry.
     *
     * @return int
     */
    protected function initMakecar()
    {
        $makecarId = $this->getRequest()->getParam('makecar_id');
        $this->coreRegistry->register(\Techies\Carbrand\Controller\RegistryConstants::CURRENT_MAKECAR_ID, $makecarId);

        return $makecarId;
    }

    /**
     * Edit or create Make
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $makecarId = $this->initMakecar();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Carbrand::carbrand_makecar');
        $resultPage->getConfig()->getTitle()->prepend(__('Makes'));
        $resultPage->addBreadcrumb(__('Make'), __('Make'));
        $resultPage->addBreadcrumb(__('Makes'), __('Makes'), $this->getUrl('techies_carbrand/makecar'));

        if ($makecarId === null) {
            $resultPage->addBreadcrumb(__('New Make'), __('New Make'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Make'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Make'), __('Edit Make'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->makecarRepository->getById($makecarId)->getMakecar()
            );
        }
        return $resultPage;
    }
}
