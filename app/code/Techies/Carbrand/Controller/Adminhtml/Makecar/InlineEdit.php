<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Controller\Adminhtml\Makecar;

class InlineEdit extends \Techies\Carbrand\Controller\Adminhtml\Makecar
{
    /**
     * Core registry
     * 
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * Make repository
     * 
     * @var \Techies\Carbrand\Api\MakecarRepositoryInterface
     */
    protected $makecarRepository;

    /**
     * Page factory
     * 
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Data object processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data object helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * JSON Factory
     * 
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $jsonFactory;

    /**
     * Make resource model
     * 
     * @var \Techies\Carbrand\Model\ResourceModel\Makecar
     */
    protected $makecarResourceModel;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Techies\Carbrand\Model\ResourceModel\Makecar $makecarResourceModel
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Techies\Carbrand\Model\ResourceModel\Makecar $makecarResourceModel
    ) {
        $this->dataObjectProcessor  = $dataObjectProcessor;
        $this->dataObjectHelper     = $dataObjectHelper;
        $this->jsonFactory          = $jsonFactory;
        $this->makecarResourceModel = $makecarResourceModel;
        parent::__construct($context, $coreRegistry, $makecarRepository, $resultPageFactory);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $makecarId) {
            /** @var \Techies\Carbrand\Model\Makecar|\Techies\Carbrand\Api\Data\MakecarInterface $makecar */
            $makecar = $this->makecarRepository->getById((int)$makecarId);
            try {
                $makecarData = $postItems[$makecarId];
                $this->dataObjectHelper->populateWithArray($makecar, $makecarData, \Techies\Carbrand\Api\Data\MakecarInterface::class);
                $this->makecarResourceModel->saveAttribute($makecar, array_keys($makecarData));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithMakecarId($makecar, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithMakecarId($makecar, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithMakecarId(
                    $makecar,
                    __('Something went wrong while saving the Make.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add Make id to error message
     *
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithMakecarId(\Techies\Carbrand\Api\Data\MakecarInterface $makecar, $errorText)
    {
        return '[Make ID: ' . $makecar->getId() . '] ' . $errorText;
    }
}
