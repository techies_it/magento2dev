<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carbrand\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $carmake;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Techies\Carbrand\Block\Makecar\Make $carmake
    ) {
        $this->carmake = $carmake;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {

        $car_year =  $this->getRequest()->getParam('car_year');

        if (!empty($car_year)) {
            $nameModel = $this->carmake;
            $data = $nameModel->getCarMakeDataByYear($car_year);
            echo json_encode($data);
        }
    }

}
