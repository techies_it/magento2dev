<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Block\Adminhtml\Makecar\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     * 
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $context;

    /**
     * Make Repository
     * 
     * @var \Techies\Carbrand\Api\MakecarRepositoryInterface
     */
    protected $makecarRepository;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Techies\Carbrand\Api\MakecarRepositoryInterface $makecarRepository
    ) {
        $this->context           = $context;
        $this->makecarRepository = $makecarRepository;
    }

    /**
     * Return Make ID
     *
     * @return int|null
     */
    public function getMakecarId()
    {
        try {
            return $this->makecarRepository->getById(
                $this->context->getRequest()->getParam('makecar_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
