<?php

/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carbrand\Block\Makecar;

class Make extends \Magento\Framework\View\Element\Template {

    private $carmaker;
    private $carmodelCollectiondata;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Techies\Carbrand\Source\Makecar $carmaker, \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory $carmodelCollectiondata
    ) {
        $this->carmodelCollectiondata = $carmodelCollectiondata;
        $this->carmaker = $carmaker;
        parent::__construct($context);
    }

    /**
     * return options makeCars
     * @return array
     */
    public function getMakeCars() {
        return $this->carmaker->getAllOptions();
        // return true;
    }

    /**
     * fetch car make data by year
     * @param $year
     * @return array
     */
    public function getCarMakeDataByYear($year) {

        $nameModel = $this->carmodelCollectiondata->create();
        $nameModel->getSelect()
                ->join(array('carmodeldata' => 'techies_carbrand_makecar'), 'carmodeldata.makecar_id= main_table.makecar', array('carmodeldata.makecar_id as makebrandid', 'carmodeldata.makecar as makename')
        );
        $nameModel->addFieldToFilter('main_table.modelyears', ['like' => '%' . $year . '%']);
        $nameModel->getSelect()->order('makename ASC');
        $nameModel->getSelect()->group('makename');

        $data = array();
        if (count($nameModel) > 0) {
            foreach ($nameModel as $val) {
                $data[$val['makebrandid']] = $val['makename'];
            }
        }

        return $data;
    }

}
