<?php

/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carbrand\Model\Makecar;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider {

    /**
     * Loaded data cache
     * 
     * @var array
     */
    protected $loadedData;

    /**
     * Data persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     * 
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
    $name, $primaryFieldName, $requestFieldName, \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory $collectionFactory, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, array $meta = [], array $data = []
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData() {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var \Techies\Carbrand\Model\Makecar $makecar */
        foreach ($items as $makecar) {
            $this->loadedData[$makecar->getId()] = $makecar->getData();
        }
        $data = $this->dataPersistor->get('techies_carbrand_makecar');
        if (!empty($data)) {
            $makecar = $this->collection->getNewEmptyItem();
            $makecar->setData($data);
            $this->loadedData[$makecar->getId()] = $makecar->getData();
            $this->dataPersistor->clear('techies_carbrand_makecar');
        }
        return $this->loadedData;
    }

}
