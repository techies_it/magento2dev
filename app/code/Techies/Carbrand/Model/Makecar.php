<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Model;

/**
 * @method \Techies\Carbrand\Model\ResourceModel\Makecar _getResource()
 * @method \Techies\Carbrand\Model\ResourceModel\Makecar getResource()
 */
class Makecar extends \Magento\Framework\Model\AbstractModel implements \Techies\Carbrand\Api\Data\MakecarInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'techies_carbrand_makecar';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'techies_carbrand_makecar';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'makecar';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Techies\Carbrand\Model\ResourceModel\Makecar::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Make id
     *
     * @return array
     */
    public function getMakecarId()
    {
        return $this->getData(\Techies\Carbrand\Api\Data\MakecarInterface::MAKECAR_ID);
    }

    /**
     * set Make id
     *
     * @param int $makecarId
     * @return \Techies\Carbrand\Api\Data\MakecarInterface
     */
    public function setMakecarId($makecarId)
    {
        return $this->setData(\Techies\Carbrand\Api\Data\MakecarInterface::MAKECAR_ID, $makecarId);
    }

    /**
     * set Make Car
     *
     * @param mixed $makecar
     * @return \Techies\Carbrand\Api\Data\MakecarInterface
     */
    public function setMakecar($makecar)
    {
        return $this->setData(\Techies\Carbrand\Api\Data\MakecarInterface::MAKECAR, $makecar);
    }

    /**
     * get Make Car
     *
     * @return string
     */
    public function getMakecar()
    {
        return $this->getData(\Techies\Carbrand\Api\Data\MakecarInterface::MAKECAR);
    }
}
