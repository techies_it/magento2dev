<?php
/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carbrand\Model;

class MakecarRepository implements \Techies\Carbrand\Api\MakecarRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $instances = [];

    /**
     * Make resource model
     * 
     * @var \Techies\Carbrand\Model\ResourceModel\Makecar
     */
    protected $resource;

    /**
     * Make collection factory
     * 
     * @var \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory
     */
    protected $makecarCollectionFactory;

    /**
     * Make interface factory
     * 
     * @var \Techies\Carbrand\Api\Data\MakecarInterfaceFactory
     */
    protected $makecarInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Techies\Carbrand\Api\Data\MakecarSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Techies\Carbrand\Model\ResourceModel\Makecar $resource
     * @param \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory $makecarCollectionFactory
     * @param \Techies\Carbrand\Api\Data\MakecarInterfaceFactory $makecarInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Carbrand\Api\Data\MakecarSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Techies\Carbrand\Model\ResourceModel\Makecar $resource,
        \Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory $makecarCollectionFactory,
        \Techies\Carbrand\Api\Data\MakecarInterfaceFactory $makecarInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Techies\Carbrand\Api\Data\MakecarSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource                 = $resource;
        $this->makecarCollectionFactory = $makecarCollectionFactory;
        $this->makecarInterfaceFactory  = $makecarInterfaceFactory;
        $this->dataObjectHelper         = $dataObjectHelper;
        $this->searchResultsFactory     = $searchResultsFactory;
    }

    /**
     * Save Make.
     *
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @return \Techies\Carbrand\Api\Data\MakecarInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Carbrand\Api\Data\MakecarInterface $makecar)
    {
        /** @var \Techies\Carbrand\Api\Data\MakecarInterface|\Magento\Framework\Model\AbstractModel $makecar */
        try {
            $this->resource->save($makecar);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Make: %1',
                $exception->getMessage()
            ));
        }
        return $makecar;
    }

    /**
     * Retrieve Make.
     *
     * @param int $makecarId
     * @return \Techies\Carbrand\Api\Data\MakecarInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($makecarId)
    {
        if (!isset($this->instances[$makecarId])) {
            /** @var \Techies\Carbrand\Api\Data\MakecarInterface|\Magento\Framework\Model\AbstractModel $makecar */
            $makecar = $this->makecarInterfaceFactory->create();
            $this->resource->load($makecar, $makecarId);
            if (!$makecar->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Make doesn\'t exist'));
            }
            $this->instances[$makecarId] = $makecar;
        }
        return $this->instances[$makecarId];
    }

    /**
     * Retrieve Makes matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Carbrand\Api\Data\MakecarSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Techies\Carbrand\Api\Data\MakecarSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Techies\Carbrand\Model\ResourceModel\Makecar\Collection $collection */
        $collection = $this->makecarCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'makecar_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Techies\Carbrand\Api\Data\MakecarInterface[] $makecars */
        $makecars = [];
        /** @var \Techies\Carbrand\Model\Makecar $makecar */
        foreach ($collection as $makecar) {
            /** @var \Techies\Carbrand\Api\Data\MakecarInterface $makecarDataObject */
            $makecarDataObject = $this->makecarInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $makecarDataObject,
                $makecar->getData(),
                \Techies\Carbrand\Api\Data\MakecarInterface::class
            );
            $makecars[] = $makecarDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($makecars);
    }

    /**
     * Delete Make.
     *
     * @param \Techies\Carbrand\Api\Data\MakecarInterface $makecar
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Carbrand\Api\Data\MakecarInterface $makecar)
    {
        /** @var \Techies\Carbrand\Api\Data\MakecarInterface|\Magento\Framework\Model\AbstractModel $makecar */
        $id = $makecar->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($makecar);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Make %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Make by ID.
     *
     * @param int $makecarId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($makecarId)
    {
        $makecar = $this->getById($makecarId);
        return $this->delete($makecar);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Techies\Carbrand\Model\ResourceModel\Makecar\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Techies\Carbrand\Model\ResourceModel\Makecar\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
