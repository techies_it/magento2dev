<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Model;

/**
 * @method \Techies\Carmodel\Model\ResourceModel\Model _getResource()
 * @method \Techies\Carmodel\Model\ResourceModel\Model getResource()
 */
class Model extends \Magento\Framework\Model\AbstractModel implements \Techies\Carmodel\Api\Data\ModelInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'techies_carmodel_model';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'techies_carmodel_model';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'model';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Techies\Carmodel\Model\ResourceModel\Model::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Carmodel id
     *
     * @return array
     */
    public function getModelId()
    {
        return $this->getData(\Techies\Carmodel\Api\Data\ModelInterface::MODEL_ID);
    }

    /**
     * set Carmodel id
     *
     * @param int $modelId
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     */
    public function setModelId($modelId)
    {
        return $this->setData(\Techies\Carmodel\Api\Data\ModelInterface::MODEL_ID, $modelId);
    }

    /**
     * set Car Model
     *
     * @param mixed $carmodel
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     */
    public function setCarmodel($carmodel)
    {
        return $this->setData(\Techies\Carmodel\Api\Data\ModelInterface::CARMODEL, $carmodel);
    }
     public function setModelyears($modelyears)
    {
         
        return $this->setData(\Techies\Carmodel\Api\Data\ModelInterface::MODELYEARS, $modelyears);
    }
    public function setSubmodels($submodels)
    {
         
        return $this->setData(\Techies\Carmodel\Api\Data\ModelInterface::SUBMODELS, $submodels);
    }

    /**
     * get Car Model
     *
     * @return string
     */
    public function getCarmodel()
    {
        return $this->getData(\Techies\Carmodel\Api\Data\ModelInterface::CARMODEL);
    }
    public function getModelyears()
    {
        return $this->getData(\Techies\Carmodel\Api\Data\ModelInterface::MODELYEARS);
    }
     public function getSubmodels()
    {
        return $this->getData(\Techies\Carmodel\Api\Data\ModelInterface::SUBMODELS);
    }

    /**
     * set Make Car
     *
     * @param mixed $makecar
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     */
    public function setMakecar($makecar)
    {
        return $this->setData(\Techies\Carmodel\Api\Data\ModelInterface::MAKECAR, $makecar);
    }

    /**
     * get Make Car
     *
     * @return string
     */
    public function getMakecar()
    {
        return $this->getData(\Techies\Carmodel\Api\Data\ModelInterface::MAKECAR);
    }

    /**
     * set Seat
     *
     * @param mixed $seats
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     */
    public function setSeats($seats)
    {
        return $this->setData(\Techies\Carmodel\Api\Data\ModelInterface::SEATS, $seats);
    }

    /**
     * get Seat
     *
     * @return string
     */
    public function getSeats()
    {
        return $this->getData(\Techies\Carmodel\Api\Data\ModelInterface::SEATS);
    }
}
