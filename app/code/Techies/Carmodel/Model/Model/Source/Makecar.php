<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Model\Model\Source;

class Makecar implements \Magento\Framework\Option\ArrayInterface {

    const SELECT = 1;
    const HONDA = 2;
    const BMW = 3;

    protected $brandcars;

    public function __construct(
    \Techies\Carbrand\Source\Makecar $brandcars
    ) {
        $this->brandCars = $brandcars;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray() {
        $options = $this->brandCars->getAllOptions();
        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptions() {
        $options = $this->brandCars->getAllOptions();
        return $options;
    }

}
