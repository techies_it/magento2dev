<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Model\Model\Source;

class Modelyears implements \Magento\Framework\Option\ArrayInterface {

    const SELECT = 1;
    const REAR = 2;

    protected $scopeConfig;

    public function __construct(
    \Techies\Cardetail\Block\CategoryProduct $scopeConfig
    ) {

        $this->scopeConfig = $scopeConfig;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray() {

        return $this->scopeConfig->getYearsFromCoreConfig();
    }

}
