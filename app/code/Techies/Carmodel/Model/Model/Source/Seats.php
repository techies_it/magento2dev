<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Model\Model\Source;

class Seats implements \Magento\Framework\Option\ArrayInterface {

    const SELECT = 1;
    const REAR = 2;

    protected $location;

    public function __construct(
    \Magento\Catalog\Model\Product\Attribute\Repository $location
    ) {

        $this->location = $location;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray() {
        $location = $this->location;
        $atrributesRepository = $location->create();
        $selectOptions = $atrributesRepository->get('location')->getOptions();
        foreach ($selectOptions as $selectOption) {
            $data[] = $selectOption->getData();
        }
        return $data;
    }

}
