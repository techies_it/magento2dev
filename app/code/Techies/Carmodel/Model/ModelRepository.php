<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Model;

class ModelRepository implements \Techies\Carmodel\Api\ModelRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $instances = [];

    /**
     * Carmodel resource model
     * 
     * @var \Techies\Carmodel\Model\ResourceModel\Model
     */
    protected $resource;

    /**
     * Carmodel collection factory
     * 
     * @var \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory
     */
    protected $modelCollectionFactory;

    /**
     * Carmodel interface factory
     * 
     * @var \Techies\Carmodel\Api\Data\ModelInterfaceFactory
     */
    protected $modelInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Techies\Carmodel\Api\Data\ModelSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Techies\Carmodel\Model\ResourceModel\Model $resource
     * @param \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory $modelCollectionFactory
     * @param \Techies\Carmodel\Api\Data\ModelInterfaceFactory $modelInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Carmodel\Api\Data\ModelSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Techies\Carmodel\Model\ResourceModel\Model $resource,
        \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory $modelCollectionFactory,
        \Techies\Carmodel\Api\Data\ModelInterfaceFactory $modelInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Techies\Carmodel\Api\Data\ModelSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource               = $resource;
        $this->modelCollectionFactory = $modelCollectionFactory;
        $this->modelInterfaceFactory  = $modelInterfaceFactory;
        $this->dataObjectHelper       = $dataObjectHelper;
        $this->searchResultsFactory   = $searchResultsFactory;
    }

    /**
     * Save Carmodel.
     *
     * @param \Techies\Carmodel\Api\Data\ModelInterface $model
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Carmodel\Api\Data\ModelInterface $model)
    {
        /** @var \Techies\Carmodel\Api\Data\ModelInterface|\Magento\Framework\Model\AbstractModel $model */
        try {
            $this->resource->save($model);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Carmodel: %1',
                $exception->getMessage()
            ));
        }
        return $model;
    }

    /**
     * Retrieve Carmodel.
     *
     * @param int $modelId
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($modelId)
    {
        if (!isset($this->instances[$modelId])) {
            /** @var \Techies\Carmodel\Api\Data\ModelInterface|\Magento\Framework\Model\AbstractModel $model */
            $model = $this->modelInterfaceFactory->create();
            $this->resource->load($model, $modelId);
            if (!$model->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Carmodel doesn\'t exist'));
            }
            $this->instances[$modelId] = $model;
        }
        return $this->instances[$modelId];
    }

    /**
     * Retrieve Carmodels matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Carmodel\Api\Data\ModelSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Techies\Carmodel\Api\Data\ModelSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Techies\Carmodel\Model\ResourceModel\Model\Collection $collection */
        $collection = $this->modelCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'model_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Techies\Carmodel\Api\Data\ModelInterface[] $models */
        $models = [];
        /** @var \Techies\Carmodel\Model\Model $model */
        foreach ($collection as $model) {
            /** @var \Techies\Carmodel\Api\Data\ModelInterface $modelDataObject */
            $modelDataObject = $this->modelInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $modelDataObject,
                $model->getData(),
                \Techies\Carmodel\Api\Data\ModelInterface::class
            );
            $models[] = $modelDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($models);
    }

    /**
     * Delete Carmodel.
     *
     * @param \Techies\Carmodel\Api\Data\ModelInterface $model
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Carmodel\Api\Data\ModelInterface $model)
    {
        /** @var \Techies\Carmodel\Api\Data\ModelInterface|\Magento\Framework\Model\AbstractModel $model */
        $id = $model->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($model);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Carmodel %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Carmodel by ID.
     *
     * @param int $modelId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($modelId)
    {
        $model = $this->getById($modelId);
        return $this->delete($model);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Techies\Carmodel\Model\ResourceModel\Model\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Techies\Carmodel\Model\ResourceModel\Model\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
