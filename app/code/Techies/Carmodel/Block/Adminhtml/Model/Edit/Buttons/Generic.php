<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Block\Adminhtml\Model\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     * 
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $context;

    /**
     * Carmodel Repository
     * 
     * @var \Techies\Carmodel\Api\ModelRepositoryInterface
     */
    protected $modelRepository;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository
    ) {
        $this->context         = $context;
        $this->modelRepository = $modelRepository;
    }

    /**
     * Return Carmodel ID
     *
     * @return int|null
     */
    public function getModelId()
    {
        try {
            return $this->modelRepository->getById(
                $this->context->getRequest()->getParam('model_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
