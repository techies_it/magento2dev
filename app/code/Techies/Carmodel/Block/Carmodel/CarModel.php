<?php

/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Block\Carmodel;

class CarModel extends \Magento\Framework\View\Element\Template {

    private $product;
    private $carmodelCollection;
    private $carmodel;
    private $catproduct;
    protected $seatnotes;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Model\Product $product, \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory $carmodelCollection, \Techies\Carmodel\Model\ResourceModel\Model $carmodel, \Magento\Catalog\Model\Product $catproduct, \Techies\Seatnote\Block\Note\Notes $seatnotes
    ) {

        $this->product = $product;
        $this->carmodelCollection = $carmodelCollection;
        $this->carmodel = $carmodel;
        $this->catproduct = $catproduct;
        $this->seatnotes = $seatnotes;
        parent::__construct($context);
    }

    /**
     * fetch car data by year, make arguments
     * @param $year
     * @param $carmake
     * @return mixed
     */
    public function getCarModelsDataByYearMake($year, $carmake) {
        $nameModel = $this->carmodelCollection->create();
        $nameModel->addFieldToFilter('makecar', ['eq' => $carmake]);
        $nameModel->addFieldToFilter('modelyears', ['like' => '%' . $year . '%']);
        $nameModel->getSelect()->group('carmodel');
        $nameModel->getSelect()->order('carmodel ASC');
        return $nameModel->toArray();
    }

    /**
     * fetch car data by year, make, model arguments
     * @param $year
     * @param $carmake
     * @param $model
     * @return array
     */
    public function getCarDataByYearMakeModel($year, $carmake, $model) {

        $car = array();

        $getCarId = $this->carmodel->getModelCarmodelById($model);

        $nameModel = $this->carmodelCollection->create();
        $nameModel->addFieldToFilter('makecar', ['eq' => $carmake]);
        $nameModel->addFieldToFilter('modelyears', ['like' => '%' . $year . '%']);
        $nameModel->addFieldToFilter('carmodel', ['eq' => $getCarId]);
        $nameModel->getSelect()->group('location');
        $nameModel->getSelect()->group('seatid');
        $nameModel->getSelect()->order('carmodel ASC');
        $nameModel->getSelect()->order('location ASC');

        $data = $nameModel->toArray();
        if (isset($data['items']) && count($data['items']) > 0) {
            foreach ($data['items'] as $k => $val) {
                // get location area seats;
                $locationId = $this->getLocations(strtoupper($val['location']));
                if (!empty($locationId)) {
                    $car[$locationId][] = $val;
                }
            }
        }

        return $car;
    }

    /**
     * fetch data of products by product id and filters by car model
     * @param $id
     * @param $model
     * @return array
     */
    public function getProductsByIdandModel($id, $model) {
        $location = array();
        $product = array();
        if (!empty($id) && !empty($model)) {
            $configProduct = $this->catproduct->load($id);
            $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);
            $childData = $_children;
            if (count($model) > 0 && count($childData) > 0) {
                foreach ($childData as $k => $val) {
                    $seatLocation = $val->getLocation();
                    if (isset($seatLocation) && !empty($seatLocation)) {
                        // location id
                        if (isset($model[$seatLocation]) && count($model[$seatLocation]) > 0) {
                            $location[$seatLocation] = $val->getId(); // product id
                            $model_id = '';
                            $makecar = '';
                            $submodels = '';
                            $viehicletype = '';
                            $seatid = '';
                            $carmodel = '';
                            $modelyears = '';
                            $bodytype = '';
                            $sn = '';
                            $locationdata = '';
                            $pattern = '';
                            $doors = '';
                            foreach ($model[$seatLocation] as $key => $values) {

                                $model_id .= $values['model_id'] . ',';
                                $carmodel .= $values['carmodel'] . ',';
                                $makecar .= $values['makecar'] . ',';
                                $modelyears .= $values['modelyears'] . ',';
                                $submodels .= $values['submodels'] . ',';
                                $bodytype .= $values['bodytype'] . ',';
                                $viehicletype .= $values['viehicletype'] . ',';
                                $sn .= $values['sn'] . ',';
                                $seatid .= $values['seatid'] . ',';
                                $locationdata .= $values['location'] . ',';
                                $pattern = $values['pattern'];
                                $doors .= $values['doors'] . ',';
                            }


                            $product[$seatLocation] = array(
                                'product_id' => implode(",", array_unique(explode(",", $val->getId()))),
                                'location_id' => implode(",", array_unique(explode(",", $seatLocation))),
                                'model_id' => implode(",", array_unique(explode(",", substr($model_id, 0, -1)))),
                                'carmodel' => implode(",", array_unique(explode(",", substr($carmodel, 0, -1)))),
                                'makecar' => implode(",", array_unique(explode(",", substr($makecar, 0, -1)))),
                                'modelyears' => implode(",", array_unique(explode(",", substr($modelyears, 0, -1)))),
                                'submodels' => implode(",", array_unique(explode(",", substr($submodels, 0, -1)))),
                                'bodytype' => implode(",", array_unique(explode(",", substr($bodytype, 0, -1)))),
                                'viehicletype' => implode(",", array_unique(explode(",", substr($viehicletype, 0, -1)))),
                                'sn' => implode(",", array_unique(explode(",", substr($sn, 0, -1)))),
                                'seatid' => implode(",", array_unique(explode(",", substr($seatid, 0, -1)))),
                                'location' => implode(",", array_unique(explode(",", substr($locationdata, 0, -1)))),
                                'pattern' => $pattern, //implode(",", array_unique(explode(",", $pattern))),
                                'doors' => implode(",", array_unique(explode(",", substr($doors, 0, -1)))),
                                'parent_product' => $configProduct->getId(),
                            );

                            $opt = $configProduct->getOptions();
                            if (count($opt) > 0) {
                                foreach ($configProduct->getOptions() as $_option) {
                                    $values = $_option->getValues();
                                    $title = $_option->getTitle();
                                    if ($title == 'Color') {
                                        foreach ($values as $option) {
                                            $product[$seatLocation]['color'][] = array('price' => $option->getPrice(), 'title' => $option->getTitle(), 'option_id' => $option->getOptionId(), 'option_type_id' => $option->getOptionTypeId());
                                        }
                                    } else {
                                        $product[$seatLocation][str_replace(' ', '', $_option->getTitle())] = $_option->getOptionId();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        ksort($product);
        return $product;
    }

    /**
     * return location like (seat location)
     * @param $arid
     * @return bool|mixed
     */
    public function getLocations($arid) {
        $arr = array('FRONT' => 10, 'MIDDLE' => 11, 'MIDDLE2' => 12, 'REAR' => 13);

        if (isset($arr[$arid]) && !empty($arr[$arid])) {
            return $arr[strtoupper($arid)];
        } else {
            return false;
        }
    }

    /**
     * fetch seat note data by year, make,model,seatid,location
     * @param $year
     * @param $carmake
     * @param $model
     * @param $seatid
     * @param $localtion
     * @return array
     */
    public function getSeatNoteByStyleId($year, $carmake, $model, $seatid, $localtion) {

        $car = array();
        if (!empty($seatid)) {
            $getCarId = $this->carmodel->getModelCarmodelById($model);
            $locationSeat = explode(",", $localtion);

            $nameModel = $this->carmodelCollection->create();
            $nameModel->addFieldToFilter('makecar', ['eq' => $carmake]);
            $nameModel->addFieldToFilter('modelyears', ['like' => '%' . $year . '%']);
            $nameModel->addFieldToFilter('carmodel', ['eq' => $getCarId]);
            $nameModel->addFieldToFilter('seatid', ['eq' => $seatid]);
            $nameModel->addFieldToFilter('location', ['eq' => $locationSeat[0]]);
            $nameModel->getSelect()->group('location');
            $nameModel->getSelect()->group('seatid');
            $nameModel->getSelect()->order('carmodel ASC');
            $nameModel->getSelect()->order('location ASC');
            $notes = array();
            $data = $nameModel->getFirstItem();
            if (isset($data) && count($data) > 0) {
                $car['location'] = $data->getLocation();
                $car['makecar'] = $data->getMakecar();
                $car['modelyears'] = $data->getModelyears();
                $car['carmodel'] = $data->getCarmodel();
                $car['seatid'] = $data->getSeatid();
                $car['pattern'] = $data->getPattern();
                $car['sn'] = $data->getSn();
                if (isset($car['sn']) && !empty($car['sn'])) {
                    $itemsNotes = explode(",", $car['sn']);
                    if (is_array($itemsNotes) && count($itemsNotes) > 0) {
                        foreach ($itemsNotes as $val) {
                            if (!empty($val)) {
                                $notes[] = $val;
                            }
                        }
                        if (isset($notes) && count($notes) > 0) {

                            $notesData = $this->seatnotes->getNoteDataBYCodeArray($notes);
                            if (isset($notesData['items']) && count($notesData['items']) > 0) {
                                $car = array();
                                foreach ($notesData['items'] as $k => $val) {
                                    $car[] = array(
                                        'note_id' => $val['note_id'],
                                        'note' => $val['note'],
                                        'description' => $val['description'],
                                        'pattern' => $data->getPattern());
                                }
                            }
                        }
                    }
                }
            }
        }
        return $car;
    }

    /**
     * get data seat notes on product view page
     * @param $year
     * @param $carmake
     * @param $model
     * @param $seatid
     * @param $localtion
     * @return array
     */
    public function getSeatNoteByStyleIdProductView($year, $carmake, $model, $seatid, $localtion) {

        $car = array();
        $notes = array();
        if (!empty($seatid)) {
            $nameModel = $this->carmodelCollection->create();
            $nameModel->addFieldToFilter('makecar', ['eq' => $carmake]);
            $nameModel->addFieldToFilter('modelyears', ['like' => '%' . $year . '%']);
            $nameModel->addFieldToFilter('carmodel', ['eq' => $model]);
            $nameModel->addFieldToFilter('seatid', ['eq' => $seatid]);
            $nameModel->addFieldToFilter('location', ['eq' => $localtion]);
            $nameModel->getSelect()->group('location');
            $nameModel->getSelect()->group('seatid');
            $nameModel->getSelect()->order('carmodel ASC');
            $nameModel->getSelect()->order('location ASC');
            $nameModel->getSelect();
            $data = $nameModel->getFirstItem();
            if (isset($data) && count($data) > 0) {
                $car['location'] = $data->getLocation();
                $car['makecar'] = $data->getMakecar();
                $car['modelyears'] = $data->getModelyears();
                $car['carmodel'] = $data->getCarmodel();
                $car['seatid'] = $data->getSeatid();
                $car['sn'] = $data->getSn();
                if (isset($car['sn']) && !empty($car['sn'])) {
                    $itemsNotes = explode(",", $car['sn']);
                    if (is_array($itemsNotes) && count($itemsNotes) > 0) {
                        foreach ($itemsNotes as $val) {
                            if (!empty($val)) {
                                $notes[] = $val;
                            }
                        }
                        if (isset($notes) && count($notes) > 0) {
                            $notesData = $this->seatnotes->getNoteDataBYCodeArray($notes);
                            if (isset($notesData['items']) && count($notesData['items']) > 0) {
                                $car = array();
                                foreach ($notesData['items'] as $k => $val) {
                                    $car[] = array(
                                        'note_id' => $val['note_id'],
                                        'note' => $val['note'],
                                        'description' => $val['description'],
                                        'pattern' => $data->getPattern(),
                                        'seatid' => $data->getSeatid(),
                                        'sn' => $data->getSn()
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        return $car;
    }

}
