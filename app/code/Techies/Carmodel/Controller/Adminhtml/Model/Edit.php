<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Controller\Adminhtml\Model;

class Edit extends \Techies\Carmodel\Controller\Adminhtml\Model
{
    /**
     * Initialize current Carmodel and set it in the registry.
     *
     * @return int
     */
    protected function initModel()
    {
        $modelId = $this->getRequest()->getParam('model_id');
        $this->coreRegistry->register(\Techies\Carmodel\Controller\RegistryConstants::CURRENT_MODEL_ID, $modelId);

        return $modelId;
    }

    /**
     * Edit or create Carmodel
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $modelId = $this->initModel();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Carmodel::carmodel_model');
        $resultPage->getConfig()->getTitle()->prepend(__('Carmodels'));
        $resultPage->addBreadcrumb(__('CarModel'), __('CarModel'));
        $resultPage->addBreadcrumb(__('Carmodels'), __('Carmodels'), $this->getUrl('techies_carmodel/model'));

        if ($modelId === null) {
            $resultPage->addBreadcrumb(__('New Carmodel'), __('New Car model'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Car model'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Carmodel'), __('Edit Car model'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->modelRepository->getById($modelId)->getCarmodel()
            );
        }
        return $resultPage;
    }
}
