<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Controller\Adminhtml\Model;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Carmodel repository
     * 
     * @var \Techies\Carmodel\Api\ModelRepositoryInterface
     */
    protected $modelRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * Carmodel collection factory
     * 
     * @var \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->modelRepository   = $modelRepository;
        $this->filter            = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->successMessage    = $successMessage;
        $this->errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Techies\Carmodel\Api\Data\ModelInterface $model
     * @return mixed
     */
    abstract protected function massAction(\Techies\Carmodel\Api\Data\ModelInterface $model);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $model) {
                $this->massAction($model);
            }
            $this->messageManager->addSuccessMessage(__($this->successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('techies_carmodel/*/index');
        return $redirectResult;
    }
}
