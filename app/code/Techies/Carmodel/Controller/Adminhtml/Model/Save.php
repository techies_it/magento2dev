<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Controller\Adminhtml\Model;

class Save extends \Techies\Carmodel\Controller\Adminhtml\Model {

    /**
     * Carmodel factory
     * 
     * @var \Techies\Carmodel\Api\Data\ModelInterfaceFactory
     */
    protected $modelFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Techies\Carmodel\Model\UploaderPool
     */
    // protected $uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Techies\Carmodel\Api\Data\ModelInterfaceFactory $modelFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Carmodel\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Techies\Carmodel\Api\Data\ModelInterfaceFactory $modelFactory, \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor, \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
    //  \Techies\Carmodel\Model\UploaderPool $uploaderPool,
            \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->modelFactory = $modelFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        // $this->uploaderPool        = $uploaderPool;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context, $coreRegistry, $modelRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute() {
        /** @var \Techies\Carmodel\Api\Data\ModelInterface $model */
        $model = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['model_id']) ? $data['model_id'] : null;
        $data['modelyears'] = implode(",", $data['modelyears']);
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $model = $this->modelRepository->getById((int) $id);
            } else {
                unset($data['model_id']);
                $model = $this->modelFactory->create();
            }
           // $this->saveCarModelsData($_REQUEST);
            $this->dataObjectHelper->populateWithArray($model, $data, \Techies\Carmodel\Api\Data\ModelInterface::class);
            $this->modelRepository->save($model);
            $this->messageManager->addSuccessMessage(__('You saved the Car model'));
            $this->dataPersistor->clear('techies_carmodel_model');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('techies_carmodel/model/edit', ['model_id' => $model->getId()]);
            } else {
                $resultRedirect->setPath('techies_carmodel/model');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('techies_carmodel_model', $postData);
            $resultRedirect->setPath('techies_carmodel/model/edit', ['model_id' => $id]);
        } catch (\Exception $e) {
            //    echo $e;exit;
            $this->messageManager->addErrorMessage(__('There was a problem saving the Car model'));
            $this->dataPersistor->set('techies_carmodel_model', $postData);
            $resultRedirect->setPath('techies_carmodel/model/edit', ['model_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Techies\Carmodel\Model\Uploader
     * @throws \Exception
     */
    protected function getUploader($type) {
        return true; //$this->uploaderPool->getUploader($type);
    }

    public function saveCarModelsData($data) {
        echo "<pre>";
        print_r($data);
        exit;
    }

}
