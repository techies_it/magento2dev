<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Controller\Adminhtml\Model;

class Delete extends \Techies\Carmodel\Controller\Adminhtml\Model
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('model_id');
        if ($id) {
            try {
                $this->modelRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The Car model has been deleted.'));
                $resultRedirect->setPath('techies_carmodel/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The Car model no longer exists.'));
                return $resultRedirect->setPath('techies_carmodel/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('techies_carmodel/model/edit', ['model_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the Car model'));
                return $resultRedirect->setPath('techies_carmodel/model/edit', ['model_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Car model to delete.'));
        $resultRedirect->setPath('techies_carmodel/*/');
        return $resultRedirect;
    }
}
