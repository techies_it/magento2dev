<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Controller\Adminhtml\Model;

use Techies\Carmodel\Controller\Adminhtml\Model\XLSXReader;

//use Techies\Carbrand\Model\ResourceModel\Makecar;

class Import extends \Techies\Carmodel\Controller\Adminhtml\Model {

    /**
     * Carmodel factory
     * 
     * @var \Techies\Carmodel\Api\Data\ModelInterfaceFactory
     */
    protected $modelFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Techies\Carmodel\Model\UploaderPool
     */
    // protected $uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;
    protected $_filesystem;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Techies\Carmodel\Api\Data\ModelInterfaceFactory $modelFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Carmodel\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Techies\Carmodel\Api\ModelRepositoryInterface $modelRepository, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Techies\Carmodel\Api\Data\ModelInterfaceFactory $modelFactory, \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor, \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
    //  \Techies\Carmodel\Model\UploaderPool $uploaderPool,
            \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    //    \Techies\Carbrand\Controller\Adminhtml\Makecar\MassAction $getmakecar
    ) {
        $this->modelFactory = $modelFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        // $this->uploaderPool        = $uploaderPool;
        $this->dataPersistor = $dataPersistor;

        parent::__construct($context, $coreRegistry, $modelRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute() {
        exit;
        require_once BP."/lib/web/phpexcel/simplexlsx.class.php";
        if ( $xlsx = \SimpleXLSX::parse(BP.'/pub/media/import/ACES.xlsx') ) {
            $worksheet_index = 0;
//            echo count($xlsx->rows(1));
//            list($num_cols, $num_rows) = $xlsx->dimension($worksheet_index);
//            echo $num_cols;
//            echo "<br>";
//            echo $num_rows;
//            exit;

//            echo "<table>";
//            foreach( $xlsx->rows($worksheet_index) as $r ) {
//                echo '<tr><td>'.implode('</td><td>', $r ).'</td></tr>';
//            }
//            echo "</table>";
//            exit;
            $current_row = 0;
            foreach( $xlsx->rows($worksheet_index) as $result ) {
                // check header row
                if($current_row == 0){
                    $current_row++;
                    continue;
                }
//                if($current_row>20){
//                    exit;
//                }
//                $current_row++;
                //echo "<br>current_row = ".$current_row."<br>";
                $makeId = 0;
//                $modeId = 0;
                if (isset($result[1]) && !empty($result[1])) {
                    $makeId = $this->getCarMake(ucwords($result[1]));
                }
                if (isset($result[2]) && !empty($result[2])) {
                    $seat_notes = array();
                    for($i=8; $i<=17; $i++){
                        if($result[$i]){
                            $seat_notes[] = $result[$i];
                        }
                    }
                    $moduleCar = array('carmodel' => ucwords($result[2]),
                        'modelyears' => $result[0],
                        'submodels' => $result[3],
                        'makecar' => $makeId,
                        'body_type' => $result[4],
                        'vehicle_type' => $result[5],
                        'numdoors' => $result[6],
                        'drive_type' => $result[7],
                        'seat_notes' => $seat_notes,
//                        'sn2' => $result[9],
//                        'sn3' => $result[10],
//                        'sn4' => $result[11],
//                        'sn5' => $result[12],
//                        'sn6' => $result[13],
//                        'sn7' => $result[14],
//                        'sn8' => $result[15],
//                        'sn9' => $result[16],
//                        'sn10' => $result[17],
                        'seat_id' => $result[18],
                        'location' => $result[19],
                        'pattern' => $result[20]
                        );
                    if (!empty($makeId)) {
                        $modeId = $this->getCarModel($moduleCar);
                    }
                }
//                if (!empty($modeId) && !empty($makeId)) {
//
//                }
            }
        } else {
            echo \SimpleXLSX::parse_error();
        }
        //exit;
    }

    public function getCarMake($name) {
        $makeId = 0;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if (isset($name) && !empty($name)) {
            $carMake = $objectManager->get('\Techies\Carbrand\Model\ResourceModel\Makecar\CollectionFactory')->create();
            $carMake->addFieldToFilter('makecar', ['eq' => $name]);
            $data = $carMake->getFirstItem();
            if (isset($data) && !empty($data)) {
                $makeId = $data->getId();
            }
        }
        if (empty($makeId)) {
            $carMake = $objectManager->get('Techies\Carbrand\Api\Data\MakecarInterfaceFactory')->create();
            $module = array('makecar' => $name);
            $data = $carMake->setData($module);
            $carMake->save($data);
            $makeId = $carMake->getId();
        }
        return $makeId;
    }

    public function getCarModel($data) {
        $modelId = 0;
        if (isset($data) && !empty($data)) {
            $carModel = $data['carmodel'];
            $makeCarId = $data['makecar'];
            $bodytype = $data['body_type'];
            $vehicletype = $data['vehicle_type'];
            $numdoors = $data['numdoors'];
            $drivetype = $data['drive_type'];
            $seatid = $data['seat_id'];
            $location = $data['location'];
            $pattern = $data['pattern'];
            $modelyears = $data['modelyears'];
            $submodels = $data['submodels'];
            $module = array();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            if (isset($carModel) && !empty($carModel)) {

                $carModelObj = $objectManager->get('\Techies\Carmodel\Model\ResourceModel\Model\CollectionFactory')->create();
                $carModelObj->addFieldToFilter('carmodel', ['eq' => $carModel]);
                $carModelObj->addFieldToFilter('makecar', ['eq' => $makeCarId]);
//                $carModelObj->addFieldToFilter('modelyears', ['eq' => $carModel]);
                $carModelObj->addFieldToFilter('bodytype', ['eq' => $bodytype]);
                $carModelObj->addFieldToFilter('viehicletype', ['eq' => $vehicletype]);
                $carModelObj->addFieldToFilter('door', ['eq' => $numdoors]);
                $carModelObj->addFieldToFilter('drivetype', ['eq' => $drivetype]);
                $carModelObj->addFieldToFilter('seatid', ['eq' => $seatid]);
                $carModelObj->addFieldToFilter('location', ['eq' => $location]);
                $carModelObj->addFieldToFilter('pattern', ['eq' => $pattern]);
               // echo $carModelObj->getSelect();
                $carModelData = $carModelObj->getFirstItem();
//                echo "<pre>"; print_r($carModelObj->getSize());
//                exit;
               // echo $carModelObj->getSize();
                if ($carModelData->getId()) {
                   // echo "found:".$carModelData->getId()."<br>";
                    $carModelId = $carModelData->getId();

                    if (!empty($modelyears)) {
                        $years = explode(",", $carModelData->getModelyears());
                        $years[] = $modelyears;
                        $years = implode(",", array_unique($years));
                        $module['modelyears'] = $years;
                        $carModelData->setModelyears($years);
                    }

                    if (!empty($submodels)) {
                        $subModel = explode(",", $carModelData->getSubmodels());
                        $subModel[] = $submodels;
                        $subModel = implode(",", array_unique($subModel));
                        $module['submodels'] = $subModel;
                        $carModelData->setSubmodels($subModel);
                    }
                    $carModelData->save();
                    $seat_note = implode(",", array_unique($data['seat_notes']));

//                    if (isset($carModelId) && !empty($carModelId)) {
//                        $carmodel = $objectManager->get('Techies\Carmodel\Api\Data\ModelInterfaceFactory')->create();
//                        $module['model_id'] = $carModelId;
//                        $data = $carmodel->setData($module);
//                        $carmodel->save($data);
//                        $modelId = $carmodel->getId();
//                    }
                }else{
                    $carModelObj = $objectManager->get('Techies\Carmodel\Api\Data\ModelInterfaceFactory')->create();
                    $seat_note = implode(",", array_unique($data['seat_notes']));
                    $module = array('carmodel' => $carModel,
                        'makecar' => $makeCarId,
                        'modelyears' => $modelyears,
                        'bodytype' => $bodytype,
                        'viehicletype' => $vehicletype,
                        'door' => $numdoors,
                        'drivetype' => $drivetype,
                        'seatid' => $seatid,
                        'location' => $location,
                        'pattern' => $pattern,
                        'sn' => $seat_note,
                        'submodels' => $submodels
                    );
                    $carModelObj->setData($module);
                    $carModelObj->save();
                    //echo "Not found:<br>";
                }
            }
            //exit;
        }
        return $modelId;
    }

}
