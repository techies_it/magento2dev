<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Controller\Adminhtml\Model;

class MassDelete extends \Techies\Carmodel\Controller\Adminhtml\Model\MassAction
{
    /**
     * @param \Techies\Carmodel\Api\Data\ModelInterface $model
     * @return $this
     */
    protected function massAction(\Techies\Carmodel\Api\Data\ModelInterface $model)
    {
        $this->modelRepository->delete($model);
        return $this;
    }
}
