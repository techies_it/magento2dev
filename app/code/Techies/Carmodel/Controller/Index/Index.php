<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {  

    protected $_pageFactory;
    protected $carmodel;
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Techies\Carmodel\Block\Carmodel\CarModel $carmodel
            
            ) {
        $this->carmodel = $carmodel;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {
 
         $car_year =  $this->getRequest()->getParam('car_year');
         $car_make =  $this->getRequest()->getParam('car_make');
        if (!empty($car_make) && !empty($car_year)) {
            $nameModel = $this->carmodel;
           $dataCar= $nameModel->getCarModelsDataByYearMake($car_year,$car_make);
            if(count($dataCar)>0){
                 echo json_encode($dataCar);
            }
            //   echo $data->getSelect();
        }
        die();
        //  return $this->_pageFactory->create();
    }

}
