<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Api;

/**
 * @api
 */
interface ModelRepositoryInterface
{
    /**
     * Save Carmodel.
     *
     * @param \Techies\Carmodel\Api\Data\ModelInterface $model
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Carmodel\Api\Data\ModelInterface $model);

    /**
     * Retrieve Carmodel
     *
     * @param int $modelId
     * @return \Techies\Carmodel\Api\Data\ModelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($modelId);

    /**
     * Retrieve Carmodels matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Carmodel\Api\Data\ModelSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Carmodel.
     *
     * @param \Techies\Carmodel\Api\Data\ModelInterface $model
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Carmodel\Api\Data\ModelInterface $model);

    /**
     * Delete Carmodel by ID.
     *
     * @param int $modelId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($modelId);
}
