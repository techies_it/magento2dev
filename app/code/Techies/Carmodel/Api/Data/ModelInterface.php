<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Api\Data;

/**
 * @api
 */
interface ModelInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const MODEL_ID = 'model_id';

    /**
     * Car Model attribute constant
     * 
     * @var string
     */
    const CARMODEL = 'carmodel';

    /**
     * Make Car attribute constant
     * 
     * @var string
     */
    const MAKECAR = 'makecar';

    /**
     * Seat attribute constant
     * 
     * @var string
     */
    const SEATS = 'seats';
    const MODELYEARS='modelyears';
    const SUBMODELS='submodels';
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getModelId();

    /**
     * Set ID
     *
     * @param int $modelId
     * @return ModelInterface
     */
    public function setModelId($modelId);

    /**
     * Get Car Model
     *
     * @return mixed
     */
    public function getCarmodel();
    public function getModelyears();
    public function setModelyears($modelyears);
    public function getSubmodels();
    public function setSubmodels($submodels);
    /**
     * Set Car Model
     *
     * @param mixed $carmodel
     * @return ModelInterface
     */
    public function setCarmodel($carmodel);

    /**
     * Get Make Car
     *
     * @return mixed
     */
    public function getMakecar();
    
    /**
     * Set Make Car
     *
     * @param mixed $makecar
     * @return ModelInterface
     */
    public function setMakecar($makecar);

    /**
     * Get Seat
     *
     * @return mixed
     */
    public function getSeats();

    /**
     * Set Seat
     *
     * @param mixed $seats
     * @return ModelInterface
     */
    public function setSeats($seats);
}
