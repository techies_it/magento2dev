<?php
/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Carmodel\Api\Data;

/**
 * @api
 */
interface ModelSearchResultInterface
{
    /**
     * Get Carmodels list.
     *
     * @return \Techies\Carmodel\Api\Data\ModelInterface[]
     */
    public function getItems();

    /**
     * Set Carmodels list.
     *
     * @param \Techies\Carmodel\Api\Data\ModelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
