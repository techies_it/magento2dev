<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Carmodel\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {


        if (version_compare($context->getVersion(), '3.0.0', '<')) {

            $setup->startSetup();
            $table = $setup->getConnection()->newTable(
                            $setup->getTable('techies_carmodel_model_data')
                    )
                    ->addColumn(
                            'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                            ], 'ID'
                    )
                    ->addColumn(
                            'carmodel_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, [
                        'nullable' => false,
                            ], 'Carmodel ID'
                    )
                    ->addColumn(
                            'seat_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel seat_id'
                    )
                    ->addColumn(
                            'sn', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel sn'
                    )
                    ->addColumn(
                            'pattern', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'seats pattern'
                    )
                    ->addColumn(
                            'location', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel location'
                    )
                    ->addColumn(
                            'body_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel body_type'
                    )
                    ->addColumn(
                            'vehicle_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel vehicle_type'
                    )
                    ->addColumn(
                            'num_doors', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel num_doors'
                    )
                    ->addColumn(
                            'drive_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel drive_type'
                    )
                    /*  ->addColumn(
                      'fab', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable => true'], 'Carmodel fab'
                      ) */
                    ->addColumn(
                            'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [
                        'nullable' => false,
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                            ], 'Carmodel Created At'
                    )
                    ->addColumn(
                            'updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [
                        'nullable' => false,
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                            ], 'Carmodel Updated At'
                    )
                    ->setComment('Carmodel Table');
            $setup->getConnection()->createTable($table);

            $setup->getConnection()->addIndex(
                    $setup->getTable('techies_carmodel_model_data'), $setup->getIdxName(
                            $setup->getTable('techies_carmodel_model_data'), ['location'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ), ['location'], \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
            $setup->endSetup();
        }

        if (version_compare($context->getVersion(), '4.0.0', '<')) {
            $setup->startSetup();
            $tableName = $setup->getTable('techies_carmodel_model');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();

                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'bodytype', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item bodytype'
                        ]
                );

                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'viehicletype', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item viehicletype'
                        ]
                );
                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'drivetype', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item drivetype'
                        ]
                );
                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'sn', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item sn'
                        ]
                );
                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'seatid', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item seatid'
                        ]
                );
                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'location', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item location'
                        ]
                );

                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'pattern', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item pattern'
                        ]
                );
                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'pattern', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item pattern'
                        ]
                );
                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_carmodel_model'), 'door', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item doors'
                        ]
                );
            }
            $setup->endSetup();
        }
    }

}
