<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Model;

/**
 * @method \Techies\Seatstyle\Model\ResourceModel\Style _getResource()
 * @method \Techies\Seatstyle\Model\ResourceModel\Style getResource()
 */
class Style extends \Magento\Framework\Model\AbstractModel implements \Techies\Seatstyle\Api\Data\StyleInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'techies_seatstyle_style';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'techies_seatstyle_style';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'style';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Techies\Seatstyle\Model\ResourceModel\Style::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Style id
     *
     * @return array
     */
    public function getStyleId()
    {
        return $this->getData(\Techies\Seatstyle\Api\Data\StyleInterface::STYLE_ID);
    }

    /**
     * set Style id
     *
     * @param int $styleId
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     */
    public function setStyleId($styleId)
    {
        return $this->setData(\Techies\Seatstyle\Api\Data\StyleInterface::STYLE_ID, $styleId);
    }

    /**
     * set Seat Code
     *
     * @param mixed $seatcode
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     */
    public function setSeatcode($seatcode)
    {
        return $this->setData(\Techies\Seatstyle\Api\Data\StyleInterface::SEATCODE, $seatcode);
    }

    /**
     * get Seat Code
     *
     * @return string
     */
    public function getSeatcode()
    {
        return $this->getData(\Techies\Seatstyle\Api\Data\StyleInterface::SEATCODE);
    }

    /**
     * set Description
     *
     * @param mixed $description
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     */
    public function setDescription($description)
    {
        return $this->setData(\Techies\Seatstyle\Api\Data\StyleInterface::DESCRIPTION, $description);
    }

    /**
     * get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(\Techies\Seatstyle\Api\Data\StyleInterface::DESCRIPTION);
    }
}
