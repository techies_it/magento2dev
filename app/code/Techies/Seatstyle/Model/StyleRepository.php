<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Model;

class StyleRepository implements \Techies\Seatstyle\Api\StyleRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $instances = [];

    /**
     * Style resource model
     * 
     * @var \Techies\Seatstyle\Model\ResourceModel\Style
     */
    protected $resource;

    /**
     * Style collection factory
     * 
     * @var \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory
     */
    protected $styleCollectionFactory;

    /**
     * Style interface factory
     * 
     * @var \Techies\Seatstyle\Api\Data\StyleInterfaceFactory
     */
    protected $styleInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Techies\Seatstyle\Api\Data\StyleSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Techies\Seatstyle\Model\ResourceModel\Style $resource
     * @param \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory $styleCollectionFactory
     * @param \Techies\Seatstyle\Api\Data\StyleInterfaceFactory $styleInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Seatstyle\Api\Data\StyleSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Techies\Seatstyle\Model\ResourceModel\Style $resource,
        \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory $styleCollectionFactory,
        \Techies\Seatstyle\Api\Data\StyleInterfaceFactory $styleInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Techies\Seatstyle\Api\Data\StyleSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource               = $resource;
        $this->styleCollectionFactory = $styleCollectionFactory;
        $this->styleInterfaceFactory  = $styleInterfaceFactory;
        $this->dataObjectHelper       = $dataObjectHelper;
        $this->searchResultsFactory   = $searchResultsFactory;
    }

    /**
     * Save Style.
     *
     * @param \Techies\Seatstyle\Api\Data\StyleInterface $style
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Seatstyle\Api\Data\StyleInterface $style)
    {
        /** @var \Techies\Seatstyle\Api\Data\StyleInterface|\Magento\Framework\Model\AbstractModel $style */
        try {
            $this->resource->save($style);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Style: %1',
                $exception->getMessage()
            ));
        }
        return $style;
    }

    /**
     * Retrieve Style.
     *
     * @param int $styleId
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($styleId)
    {
        if (!isset($this->instances[$styleId])) {
            /** @var \Techies\Seatstyle\Api\Data\StyleInterface|\Magento\Framework\Model\AbstractModel $style */
            $style = $this->styleInterfaceFactory->create();
            $this->resource->load($style, $styleId);
            if (!$style->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Style doesn\'t exist'));
            }
            $this->instances[$styleId] = $style;
        }
        return $this->instances[$styleId];
    }

    /**
     * Retrieve Styles matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Seatstyle\Api\Data\StyleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Techies\Seatstyle\Api\Data\StyleSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Techies\Seatstyle\Model\ResourceModel\Style\Collection $collection */
        $collection = $this->styleCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'style_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Techies\Seatstyle\Api\Data\StyleInterface[] $styles */
        $styles = [];
        /** @var \Techies\Seatstyle\Model\Style $style */
        foreach ($collection as $style) {
            /** @var \Techies\Seatstyle\Api\Data\StyleInterface $styleDataObject */
            $styleDataObject = $this->styleInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $styleDataObject,
                $style->getData(),
                \Techies\Seatstyle\Api\Data\StyleInterface::class
            );
            $styles[] = $styleDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($styles);
    }

    /**
     * Delete Style.
     *
     * @param \Techies\Seatstyle\Api\Data\StyleInterface $style
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Seatstyle\Api\Data\StyleInterface $style)
    {
        /** @var \Techies\Seatstyle\Api\Data\StyleInterface|\Magento\Framework\Model\AbstractModel $style */
        $id = $style->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($style);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Style %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Style by ID.
     *
     * @param int $styleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($styleId)
    {
        $style = $this->getById($styleId);
        return $this->delete($style);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Techies\Seatstyle\Model\ResourceModel\Style\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Techies\Seatstyle\Model\ResourceModel\Style\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
