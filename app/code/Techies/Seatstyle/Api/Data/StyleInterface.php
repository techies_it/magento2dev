<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Api\Data;

/**
 * @api
 */
interface StyleInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const STYLE_ID = 'style_id';

    /**
     * Seat Code attribute constant
     * 
     * @var string
     */
    const SEATCODE = 'seatcode';

    /**
     * Description attribute constant
     * 
     * @var string
     */
    const DESCRIPTION = 'description';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getStyleId();

    /**
     * Set ID
     *
     * @param int $styleId
     * @return StyleInterface
     */
    public function setStyleId($styleId);

    /**
     * Get Seat Code
     *
     * @return mixed
     */
    public function getSeatcode();

    /**
     * Set Seat Code
     *
     * @param mixed $seatcode
     * @return StyleInterface
     */
    public function setSeatcode($seatcode);

    /**
     * Get Description
     *
     * @return mixed
     */
    public function getDescription();

    /**
     * Set Description
     *
     * @param mixed $description
     * @return StyleInterface
     */
    public function setDescription($description);
}
