<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Api\Data;

/**
 * @api
 */
interface StyleSearchResultInterface
{
    /**
     * Get Styles list.
     *
     * @return \Techies\Seatstyle\Api\Data\StyleInterface[]
     */
    public function getItems();

    /**
     * Set Styles list.
     *
     * @param \Techies\Seatstyle\Api\Data\StyleInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
