<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Api;

/**
 * @api
 */
interface StyleRepositoryInterface
{
    /**
     * Save Style.
     *
     * @param \Techies\Seatstyle\Api\Data\StyleInterface $style
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Seatstyle\Api\Data\StyleInterface $style);

    /**
     * Retrieve Style
     *
     * @param int $styleId
     * @return \Techies\Seatstyle\Api\Data\StyleInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($styleId);

    /**
     * Retrieve Styles matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Seatstyle\Api\Data\StyleSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Style.
     *
     * @param \Techies\Seatstyle\Api\Data\StyleInterface $style
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Seatstyle\Api\Data\StyleInterface $style);

    /**
     * Delete Style by ID.
     *
     * @param int $styleId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($styleId);
}
