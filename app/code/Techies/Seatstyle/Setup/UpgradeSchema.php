<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Seatstyle\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {

 

        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            $setup->startSetup();
            $tableName = $setup->getTable('techies_seatstyle_style');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();

                $setup->getConnection()->addColumn(
                        $setup->getTable('techies_seatstyle_style'), 'imgstyle', [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Item imgstyle'
                        ]
                );
            }
            $setup->endSetup();
        }
    }

}
