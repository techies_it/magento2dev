<?php

/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Seatstyle\Block\Style;

class Style extends \Magento\Framework\View\Element\Template {

    private $product;
    private $setanotecollection;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Model\Product $product, \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory $setanotecollection
    ) {
        $this->setanotecollection = $setanotecollection;
        $this->product = $product;
        parent::__construct($context);
    }

    /**
     * return seat style by seat note id
     * @param $noteId
     * @return bool
     */
    public function getStyleDataBYCode($noteId) {
        if (!empty($noteId)) {
            $nameModel = $this->setanotecollection->create();
            $nameModel->addFieldToFilter('seatcode', ['eq' => $noteId]);
            return $data = $nameModel->getFirstItem();
        } else {
            return false;
        }
    }
}
