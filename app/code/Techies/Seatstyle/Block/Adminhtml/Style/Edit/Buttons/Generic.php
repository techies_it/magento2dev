<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Block\Adminhtml\Style\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     * 
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $context;

    /**
     * Style Repository
     * 
     * @var \Techies\Seatstyle\Api\StyleRepositoryInterface
     */
    protected $styleRepository;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Techies\Seatstyle\Api\StyleRepositoryInterface $styleRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Techies\Seatstyle\Api\StyleRepositoryInterface $styleRepository
    ) {
        $this->context         = $context;
        $this->styleRepository = $styleRepository;
    }

    /**
     * Return Style ID
     *
     * @return int|null
     */
    public function getStyleId()
    {
        try {
            return $this->styleRepository->getById(
                $this->context->getRequest()->getParam('style_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
