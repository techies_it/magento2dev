<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Controller\Adminhtml\Style;

class Index extends \Techies\Seatstyle\Controller\Adminhtml\Style
{
    /**
     * Styles list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Seatstyle::style');
        $resultPage->getConfig()->getTitle()->prepend(__('Styles'));
        $resultPage->addBreadcrumb(__('Seat Style'), __('Seat Style'));
        $resultPage->addBreadcrumb(__('Styles'), __('Styles'));
        return $resultPage;
    }
}
