<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Controller\Adminhtml\Style;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Style repository
     * 
     * @var \Techies\Seatstyle\Api\StyleRepositoryInterface
     */
    protected $styleRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * Style collection factory
     * 
     * @var \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Techies\Seatstyle\Api\StyleRepositoryInterface $styleRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Techies\Seatstyle\Api\StyleRepositoryInterface $styleRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Techies\Seatstyle\Model\ResourceModel\Style\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->styleRepository   = $styleRepository;
        $this->filter            = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->successMessage    = $successMessage;
        $this->errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Techies\Seatstyle\Api\Data\StyleInterface $style
     * @return mixed
     */
    abstract protected function massAction(\Techies\Seatstyle\Api\Data\StyleInterface $style);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $style) {
                $this->massAction($style);
            }
            $this->messageManager->addSuccessMessage(__($this->successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('techies_seatstyle/*/index');
        return $redirectResult;
    }
}
