<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Controller\Adminhtml\Style;

class Save extends \Techies\Seatstyle\Controller\Adminhtml\Style
{
    /**
     * Style factory
     * 
     * @var \Techies\Seatstyle\Api\Data\StyleInterfaceFactory
     */
    protected $styleFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Techies\Seatstyle\Model\UploaderPool
     */
    protected $uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Techies\Seatstyle\Api\StyleRepositoryInterface $styleRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Techies\Seatstyle\Api\Data\StyleInterfaceFactory $styleFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Seatstyle\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Techies\Seatstyle\Api\StyleRepositoryInterface $styleRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Techies\Seatstyle\Api\Data\StyleInterfaceFactory $styleFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
       // \Techies\Seatstyle\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->styleFactory        = $styleFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper    = $dataObjectHelper;
      //  $this->uploaderPool        = $uploaderPool;
        $this->dataPersistor       = $dataPersistor;
        parent::__construct($context, $coreRegistry, $styleRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Techies\Seatstyle\Api\Data\StyleInterface $style */
        $style = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['style_id']) ? $data['style_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $style = $this->styleRepository->getById((int)$id);
            } else {
                unset($data['style_id']);
                $style = $this->styleFactory->create();
            }
            $this->dataObjectHelper->populateWithArray($style, $data, \Techies\Seatstyle\Api\Data\StyleInterface::class);
            $this->styleRepository->save($style);
            $this->messageManager->addSuccessMessage(__('You saved the Style'));
            $this->dataPersistor->clear('techies_seatstyle_style');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('techies_seatstyle/style/edit', ['style_id' => $style->getId()]);
            } else {
                $resultRedirect->setPath('techies_seatstyle/style');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('techies_seatstyle_style', $postData);
            $resultRedirect->setPath('techies_seatstyle/style/edit', ['style_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Style'));
            $this->dataPersistor->set('techies_seatstyle_style', $postData);
            $resultRedirect->setPath('techies_seatstyle/style/edit', ['style_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Techies\Seatstyle\Model\Uploader
     * @throws \Exception
     */
  /*  protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }*/
}
