<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Controller\Adminhtml\Style;

class Edit extends \Techies\Seatstyle\Controller\Adminhtml\Style
{
    /**
     * Initialize current Style and set it in the registry.
     *
     * @return int
     */
    protected function initStyle()
    {
        $styleId = $this->getRequest()->getParam('style_id');
        $this->coreRegistry->register(\Techies\Seatstyle\Controller\RegistryConstants::CURRENT_STYLE_ID, $styleId);

        return $styleId;
    }

    /**
     * Edit or create Style
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $styleId = $this->initStyle();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Seatstyle::seatstyle_style');
        $resultPage->getConfig()->getTitle()->prepend(__('Styles'));
        $resultPage->addBreadcrumb(__('Seat Style'), __('Seat Style'));
        $resultPage->addBreadcrumb(__('Styles'), __('Styles'), $this->getUrl('techies_seatstyle/style'));

        if ($styleId === null) {
            $resultPage->addBreadcrumb(__('New Style'), __('New Style'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Style'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Style'), __('Edit Style'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->styleRepository->getById($styleId)->getSeatcode()
            );
        }
        return $resultPage;
    }
}
