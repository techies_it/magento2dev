<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Controller\Adminhtml\Style;

class Delete extends \Techies\Seatstyle\Controller\Adminhtml\Style
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('style_id');
        if ($id) {
            try {
                $this->styleRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The Style has been deleted.'));
                $resultRedirect->setPath('techies_seatstyle/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The Style no longer exists.'));
                return $resultRedirect->setPath('techies_seatstyle/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('techies_seatstyle/style/edit', ['style_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the Style'));
                return $resultRedirect->setPath('techies_seatstyle/style/edit', ['style_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Style to delete.'));
        $resultRedirect->setPath('techies_seatstyle/*/');
        return $resultRedirect;
    }
}
