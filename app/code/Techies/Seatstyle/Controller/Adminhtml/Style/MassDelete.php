<?php
/**
 * Techies_Seatstyle extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatstyle
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatstyle\Controller\Adminhtml\Style;

class MassDelete extends \Techies\Seatstyle\Controller\Adminhtml\Style\MassAction
{
    /**
     * @param \Techies\Seatstyle\Api\Data\StyleInterface $style
     * @return $this
     */
    protected function massAction(\Techies\Seatstyle\Api\Data\StyleInterface $style)
    {
        $this->styleRepository->delete($style);
        return $this;
    }
}
