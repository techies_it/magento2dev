<?php

namespace Techies\SmartSearch\Block;

class SmartSearch extends \Magento\Framework\View\Element\Template
{
	protected $catalogSessionData;
	protected $eavConfig;
	protected $_productCollection;
	protected $stockprice;
	protected $_productloader;
	protected $configurable;
	
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,        
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
		\Magento\Framework\Session\SessionManagerInterface $coreSession, 
		\Magento\Catalog\Model\Session $catalogSessionData,
		\Magento\Eav\Model\Config $eavConfig,
		\Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
		\Magento\Catalog\Model\ProductRepository $_productloader,
		\Magento\CatalogInventory\Helper\Stock $stockprice,
		array $data = []
	)
	{    
		$this->catalogSessionData = $catalogSessionData;
		$this->_productCollectionFactory = $productCollectionFactory;    
		$this->_coreSession = $coreSession;
		$this->eavConfig = $eavConfig;
		$this->stockprice =$stockprice;
		$this->_productloader = $_productloader;
		$this->configurable = $configurable;
		parent::__construct($context, $data);
	}

	/**
	 * return product collection
	 * @return mixed
	 */
	public function getProductCollection()
	{
		$collection = $this->_productCollectionFactory->create();
		$collection->addAttributeToSelect('*');
		return $collection;
	}

	/**
	 * @param int $childProductId
	 * @return int
	 */
	public function getParentProductId($childProductId)
	{
		$parentConfigObject = $this->configurable->getParentIdsByChild($childProductId);
		if($parentConfigObject) {
			return $parentConfigObject[0];
		}
		return false;
	}

	/**
	 * return product data by id
	 * @param $id
	 * @return mixed
	 */
	public function getLoadProduct($id) {
		return $this->_productloader->getById($id);
	}

	/**
	 * @return string
	 */
	 public function getFormAction()
	{
		return '/fabric';
	}

	/**
	 * @return \Magento\Framework\Session\SessionManagerInterface
	 */
	public function getCoreSession() 
	{
		return $this->_coreSession;
	}

	/**
	 * @return mixed
	 */
	public function getPostData(){
		$postData = $this->getRequest()->getPost();
		return $postData;
	}

	/**
	 * @return \Magento\Catalog\Model\Session
	 */
	public function getCatalogSessionData()
	{
		return $this->catalogSessionData;
	}

	/**
	 * @return mixed
	 */
	public function getLifeStyleAttr() 
	{
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'lifestyle');
		return $options = $attribute->getSource()->getAllOptions();
	}

	/**
	 * @param AbstractCollection $collection
	 */
	public function setProductCollection(AbstractCollection $collection)
	{
		$this->_productCollection = $collection;
	}

	/**
	 * return minimum price and maximum price
	 * @return array
	 */
	public function getPriceFormColation()
	{
		$collection = $this->_productCollectionFactory->create();
		$collection->addAttributeToSelect('price')
		->setOrder('price', 'DESC');
	  	$price=array();
	  	if(isset($collection) && count($collection)>0){
	  		foreach($collection as $product){
				$p=$product->getPrice();
				if(!empty($p)){
					$price[]= number_format($p, 2, '.', '');
				}
	  		}
		}
	  	return  $arr=  array('min'=>min($price),'max'=>max($price)) ;
	}

	/**
	 * return minimum and maximum value of water_resistance attribute
	 * @return array
	 */
	public function getWaterResistanceAttr() 
	{
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'water_resistance');
		$options = $attribute->getSource()->getAllOptions();
		$optionVal=array();
		foreach($options as $val){
			if(!empty($val['value'])){
			   $optionVal[] = $val['value'];
			}
		}
		return  $arr=  array('min'=>min($optionVal),'max'=>max($optionVal)) ;
	}

	/**
	 * return minimum and maximum value of durability attribute
	 * @return array
	 */
	public function getDurabilityAttr() 
	{
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'durability');
		$options = $attribute->getSource()->getAllOptions();
		$optionVal=array();
		foreach($options as $val){
			if(!empty($val['value'])){
			   $optionVal[] = $val['value'];
			}
		}
		return  $arr=  array('min'=>min($optionVal),'max'=>max($optionVal)) ;
	}

	/**
	 * return minimum and maximum value of softness attribute
	 * @return array
	 */
	public function getSoftnessAttr() 
	{
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'softness');
		$options = $attribute->getSource()->getAllOptions();
		$optionVal=array();
		foreach($options as $val){
			if(!empty($val['value'])){
			   $optionVal[] = $val['value'];
			}
		}
		return  $arr=  array('min'=>min($optionVal),'max'=>max($optionVal)) ;
	}
}