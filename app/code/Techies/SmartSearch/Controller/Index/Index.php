<?php

namespace Techies\SmartSearch\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action{
	
	protected $resultPageFactory;
	
	protected $_coreSession;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;
        parent::__construct($context);
    }
    
    /*
	* return page title and description
	*
	*/
    public function execute()
    {
	    $resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->set("Wide Fabric Selection for Truck Seat Covers | Saddleman");
        $resultPage->getConfig()->setDescription("We use only the best fabrics for our truck seat covers: canvas, Cambridge tweed, Windsor velour, microsuede and more. Call us on 1-800-883-9919 today.");
        return $resultPage;
    }
}