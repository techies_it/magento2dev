<?php

namespace Techies\SmartSearch\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class AjaxFilter extends Action{
	
	protected $resultPageFactory;
	
	protected $_coreSession;

	public function __construct(Context $context, PageFactory $pageFactory)
	{
		$this->resultPageFactory = $pageFactory;
		parent::__construct($context);
	}

	/*
	* Ajax Filter to get products Html based on the filter selection
	*
	*/
	public function execute()
	{
		$postData =  $this->getRequest()->getParams();
		$resultPage = $this->resultPageFactory->create();
		$block = $resultPage->getLayout()
			->createBlock('Magento\Framework\View\Element\Template')
			->setTemplate('Techies_SmartSearch::filter/filter_products.phtml')
			->setAjaxData($postData)  
			->toHtml();
		echo $block;
		die();
	}
}