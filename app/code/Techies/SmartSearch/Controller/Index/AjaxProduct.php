<?php

namespace Techies\SmartSearch\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class AjaxProduct extends Action{
	
	protected $resultPageFactory;
	
	protected $_coreSession;

	public function __construct(Context $context, PageFactory $pageFactory)
	{
		$this->resultPageFactory = $pageFactory;
		parent::__construct($context);
	}

	/*
	*  get Products Block Html based on product Id 
	*
	*/
	public function execute()
	{
		$product_id =  $this->getRequest()->getParam('product_id');
		$resultPage = $this->resultPageFactory->create();
		$block = $resultPage->getLayout()
			->createBlock('Magento\Framework\View\Element\Template')
			->setTemplate('Techies_SmartSearch::ajaxproduct.phtml')
			->setAjaxData($product_id)  
			->toHtml();
		echo $block;
		die();
	}
	
}