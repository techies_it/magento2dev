<?php 
/**
 * Techies_SmartSearch extension
 * NOTICE OF LICENSE
 * 
 */
?>
<?php
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Techies_SmartSearch',
    __DIR__
);
