<?php
/**
 * Techies_IframeVehicle extension
 * 
 * @category  Techies
 * @package   Techies_IframeVehicle
 * @copyright Copyright (c) 2018
 */
namespace Techies\IframeVehicle\Block;

class IframeVehicle extends \Magento\Framework\View\Element\Template
{

public function __construct(
	\Magento\Backend\Block\Template\Context $context,        
	\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, 
	\Magento\Framework\Session\SessionManagerInterface $coreSession, 	
	array $data = []
)
{    
	$this->_productCollectionFactory = $productCollectionFactory;    
	$this->_coreSession = $coreSession;
	parent::__construct($context, $data);
}

/**
* return product collection
* @return array
*/
public function getProductCollection()
{
	$collection = $this->_productCollectionFactory->create();
	$collection->addAttributeToSelect('*');
	return $collection;
}

/**
* Fetch Product Id by SKU
* @return int
*/
public function getProductIdBySku($fabric)
{
	$collection = $this->_productCollectionFactory->create();
	$collection->addFieldToFilter('sku', ['eq' => $fabric]);
	return $collection->getFirstItem();
}
	 
/**
* return core session data
* @return array
*/	
public function getCoreSession() 
{
	return $this->_coreSession;
} 

/**
* Get Post Data
* @return array
*/
public function getPostData(){
		$postData = $this->getRequest()->getPost();
		return $postData;
	}
}