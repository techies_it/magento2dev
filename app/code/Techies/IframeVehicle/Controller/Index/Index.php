<?php
/**
 * Techies_IframeVehicle extension
 * 
 * @category  Techies
 * @package   Techies_IframeVehicle
 * @copyright Copyright (c) 2018
 */
namespace Techies\IframeVehicle\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action{
	
	protected $resultPageFactory;
	protected $_coreSession;

	public function __construct(Context $context, PageFactory $pageFactory)
	{
		$this->resultPageFactory = $pageFactory;
		parent::__construct($context);
	}

	public function execute()
	{
		$resultPage = $this->resultPageFactory->create();
		return $resultPage;
	}
}