<?php
/**
 * Techies_IframeVehicle extension
 * 
 * @category  Techies
 * @package   Techies_IframeVehicle
 * @copyright Copyright (c) 2018
 */
namespace Techies\IframeVehicle\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Techies\Carmodel\Block\Carmodel\CarModel;

class Result extends Action{
	
	protected $resultPageFactory;
	protected $carModelBlock;
	protected $productFactory;
	protected $_coreSession;

	public function __construct(Context $context, PageFactory $pageFactory, CarModel $CarModel, \Magento\Catalog\Model\ProductFactory $productFactory)
	{
		$this->resultPageFactory = $pageFactory;
		$this->carModelBlock = $CarModel;
		$this->productFactory = $productFactory;
		parent::__construct($context);
	}
	/**
	* return products Block Html based on the advanced search data. like Car Year, Car Make and Car Model.  
	* 
	*/
	public function execute()
	{
		$car_year =$this->getRequest()->getParam('car_year'); //1996
		$car_make = $this->getRequest()->getParam('car_make'); //6;
		$car_model = $this->getRequest()->getParam('car_model'); //610; 
		$fabricSku = 'fabric-'.$this->getRequest()->getParam('fabric');
		$product = $this->productFactory->create();
		$productID =  $product->getIdBySku($fabricSku);
		$data = $this->carModelBlock->getCarDataByYearMakeModel($car_year, $car_make, $car_model);
		if (count($data) > 0) {

			if (!empty($productID)) {
				$dataCar = $this->carModelBlock->getProductsByIdandModel($productID, $data);
				
				$resultPage = $this->resultPageFactory->create();
				$block = $resultPage->getLayout()
						->createBlock('Magento\Framework\View\Element\Template')
						->setTemplate('Techies_IframeVehicle::product.phtml')
						->setDataCar($dataCar)
						->toHtml();
				echo $block;
			}
		}
		die();
	}
	
}