<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Model;

class NoteRepository implements \Techies\Seatnote\Api\NoteRepositoryInterface
{
    /**
     * Cached instances
     * 
     * @var array
     */
    protected $instances = [];

    /**
     * Note resource model
     * 
     * @var \Techies\Seatnote\Model\ResourceModel\Note
     */
    protected $resource;

    /**
     * Note collection factory
     * 
     * @var \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory
     */
    protected $noteCollectionFactory;

    /**
     * Note interface factory
     * 
     * @var \Techies\Seatnote\Api\Data\NoteInterfaceFactory
     */
    protected $noteInterfaceFactory;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Search result factory
     * 
     * @var \Techies\Seatnote\Api\Data\NoteSearchResultInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * constructor
     * 
     * @param \Techies\Seatnote\Model\ResourceModel\Note $resource
     * @param \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory $noteCollectionFactory
     * @param \Techies\Seatnote\Api\Data\NoteInterfaceFactory $noteInterfaceFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Seatnote\Api\Data\NoteSearchResultInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Techies\Seatnote\Model\ResourceModel\Note $resource,
        \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory $noteCollectionFactory,
        \Techies\Seatnote\Api\Data\NoteInterfaceFactory $noteInterfaceFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Techies\Seatnote\Api\Data\NoteSearchResultInterfaceFactory $searchResultsFactory
    ) {
        $this->resource              = $resource;
        $this->noteCollectionFactory = $noteCollectionFactory;
        $this->noteInterfaceFactory  = $noteInterfaceFactory;
        $this->dataObjectHelper      = $dataObjectHelper;
        $this->searchResultsFactory  = $searchResultsFactory;
    }

    /**
     * Save Note.
     *
     * @param \Techies\Seatnote\Api\Data\NoteInterface $note
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Seatnote\Api\Data\NoteInterface $note)
    {
        /** @var \Techies\Seatnote\Api\Data\NoteInterface|\Magento\Framework\Model\AbstractModel $note */
        try {
            $this->resource->save($note);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__(
                'Could not save the Note: %1',
                $exception->getMessage()
            ));
        }
        return $note;
    }

    /**
     * Retrieve Note.
     *
     * @param int $noteId
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($noteId)
    {
        if (!isset($this->instances[$noteId])) {
            /** @var \Techies\Seatnote\Api\Data\NoteInterface|\Magento\Framework\Model\AbstractModel $note */
            $note = $this->noteInterfaceFactory->create();
            $this->resource->load($note, $noteId);
            if (!$note->getId()) {
                throw new \Magento\Framework\Exception\NoSuchEntityException(__('Requested Note doesn\'t exist'));
            }
            $this->instances[$noteId] = $note;
        }
        return $this->instances[$noteId];
    }

    /**
     * Retrieve Notes matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Seatnote\Api\Data\NoteSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Techies\Seatnote\Api\Data\NoteSearchResultInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);

        /** @var \Techies\Seatnote\Model\ResourceModel\Note\Collection $collection */
        $collection = $this->noteCollectionFactory->create();

        //Add filters from root filter group to the collection
        /** @var \Magento\Framework\Api\Search\FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $field = $sortOrder->getField();
                $collection->addOrder(
                    $field,
                    ($sortOrder->getDirection() == \Magento\Framework\Api\SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            // set a default sorting order since this method is used constantly in many
            // different blocks
            $field = 'note_id';
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Techies\Seatnote\Api\Data\NoteInterface[] $notes */
        $notes = [];
        /** @var \Techies\Seatnote\Model\Note $note */
        foreach ($collection as $note) {
            /** @var \Techies\Seatnote\Api\Data\NoteInterface $noteDataObject */
            $noteDataObject = $this->noteInterfaceFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $noteDataObject,
                $note->getData(),
                \Techies\Seatnote\Api\Data\NoteInterface::class
            );
            $notes[] = $noteDataObject;
        }
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults->setItems($notes);
    }

    /**
     * Delete Note.
     *
     * @param \Techies\Seatnote\Api\Data\NoteInterface $note
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Seatnote\Api\Data\NoteInterface $note)
    {
        /** @var \Techies\Seatnote\Api\Data\NoteInterface|\Magento\Framework\Model\AbstractModel $note */
        $id = $note->getId();
        try {
            unset($this->instances[$id]);
            $this->resource->delete($note);
        } catch (\Magento\Framework\Exception\ValidatorException $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove Note %1', $id)
            );
        }
        unset($this->instances[$id]);
        return true;
    }

    /**
     * Delete Note by ID.
     *
     * @param int $noteId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($noteId)
    {
        $note = $this->getById($noteId);
        return $this->delete($note);
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Techies\Seatnote\Model\ResourceModel\Note\Collection $collection
     * @return $this
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Techies\Seatnote\Model\ResourceModel\Note\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
        return $this;
    }
}
