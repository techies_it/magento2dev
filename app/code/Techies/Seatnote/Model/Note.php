<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Model;

/**
 * @method \Techies\Seatnote\Model\ResourceModel\Note _getResource()
 * @method \Techies\Seatnote\Model\ResourceModel\Note getResource()
 */
class Note extends \Magento\Framework\Model\AbstractModel implements \Techies\Seatnote\Api\Data\NoteInterface
{
    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'techies_seatnote_note';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'techies_seatnote_note';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'note';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Techies\Seatnote\Model\ResourceModel\Note::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get Note id
     *
     * @return array
     */
    public function getNoteId()
    {
        return $this->getData(\Techies\Seatnote\Api\Data\NoteInterface::NOTE_ID);
    }

    /**
     * set Note id
     *
     * @param int $noteId
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     */
    public function setNoteId($noteId)
    {
        return $this->setData(\Techies\Seatnote\Api\Data\NoteInterface::NOTE_ID, $noteId);
    }

    /**
     * set Note
     *
     * @param mixed $note
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     */
    public function setNote($note)
    {
        return $this->setData(\Techies\Seatnote\Api\Data\NoteInterface::NOTE, $note);
    }

    /**
     * get Note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->getData(\Techies\Seatnote\Api\Data\NoteInterface::NOTE);
    }

    /**
     * set Description
     *
     * @param mixed $description
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     */
    public function setDescription($description)
    {
        return $this->setData(\Techies\Seatnote\Api\Data\NoteInterface::DESCRIPTION, $description);
    }

    /**
     * get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(\Techies\Seatnote\Api\Data\NoteInterface::DESCRIPTION);
    }
}
