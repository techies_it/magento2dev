<?php

/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Seatnote\Block\Note;

class Notes extends \Magento\Framework\View\Element\Template {

    private $product;
    private $setanotecollection;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Model\Product $product, \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory $setanotecollection
    ) {
        $this->setanotecollection = $setanotecollection;
        $this->product = $product;
        parent::__construct($context);
    }

    /**
     * return seat notes
     * @param $noteId
     * @return bool
     */
    public function getNoteDataBYCode($noteId) {
        if (!empty($noteId)) {
            $nameModel = $this->setanotecollection->create();
            $nameModel->addFieldToFilter('note', ['eq' => $noteId]);
            return $data = $nameModel->getFirstItem();
        } else {
            return false;
        }
    }

    /**
     * return seat notes array
     * @param $arr
     * @return bool
     */
    public function getNoteDataBYCodeArray($arr) {
        if (!empty($arr)) {
            $nameModel = $this->setanotecollection->create();
            $nameModel->addFieldToFilter('note', ['in' => $arr]);
            $nameModel->getSelect()->group('note');
            $nameModel->getSelect()->order('description ASC');
            return $data = $nameModel->toArray();
        } else {
            return false;
        }
    }

}
