<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Block\Adminhtml\Note\Edit\Buttons;

class Generic
{
    /**
     * Widget Context
     * 
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $context;

    /**
     * Note Repository
     * 
     * @var \Techies\Seatnote\Api\NoteRepositoryInterface
     */
    protected $noteRepository;

    /**
     * constructor
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Techies\Seatnote\Api\NoteRepositoryInterface $noteRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Techies\Seatnote\Api\NoteRepositoryInterface $noteRepository
    ) {
        $this->context        = $context;
        $this->noteRepository = $noteRepository;
    }

    /**
     * Return Note ID
     *
     * @return int|null
     */
    public function getNoteId()
    {
        try {
            return $this->noteRepository->getById(
                $this->context->getRequest()->getParam('note_id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
