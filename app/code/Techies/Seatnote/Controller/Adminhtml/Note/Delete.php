<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Controller\Adminhtml\Note;

class Delete extends \Techies\Seatnote\Controller\Adminhtml\Note
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('note_id');
        if ($id) {
            try {
                $this->noteRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('The Note has been deleted.'));
                $resultRedirect->setPath('techies_seatnote/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $this->messageManager->addErrorMessage(__('The Note no longer exists.'));
                return $resultRedirect->setPath('techies_seatnote/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('techies_seatnote/note/edit', ['note_id' => $id]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('There was a problem deleting the Note'));
                return $resultRedirect->setPath('techies_seatnote/note/edit', ['note_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Note to delete.'));
        $resultRedirect->setPath('techies_seatnote/*/');
        return $resultRedirect;
    }
}
