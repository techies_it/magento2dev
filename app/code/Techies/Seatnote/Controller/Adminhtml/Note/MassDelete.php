<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Controller\Adminhtml\Note;

class MassDelete extends \Techies\Seatnote\Controller\Adminhtml\Note\MassAction
{
    /**
     * @param \Techies\Seatnote\Api\Data\NoteInterface $note
     * @return $this
     */
    protected function massAction(\Techies\Seatnote\Api\Data\NoteInterface $note)
    {
        $this->noteRepository->delete($note);
        return $this;
    }
}
