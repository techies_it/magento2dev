<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Controller\Adminhtml\Note;

class Index extends \Techies\Seatnote\Controller\Adminhtml\Note
{
    /**
     * Notes list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Seatnote::note');
        $resultPage->getConfig()->getTitle()->prepend(__('Notes'));
        $resultPage->addBreadcrumb(__('Seat Notes'), __('Seat Notes'));
        $resultPage->addBreadcrumb(__('Notes'), __('Notes'));
        return $resultPage;
    }
}
