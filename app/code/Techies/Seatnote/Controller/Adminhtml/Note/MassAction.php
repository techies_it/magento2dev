<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Controller\Adminhtml\Note;

abstract class MassAction extends \Magento\Backend\App\Action
{
    /**
     * Note repository
     * 
     * @var \Techies\Seatnote\Api\NoteRepositoryInterface
     */
    protected $noteRepository;

    /**
     * Mass Action filter
     * 
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * Note collection factory
     * 
     * @var \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Action success message
     * 
     * @var string
     */
    protected $successMessage;

    /**
     * Action error message
     * 
     * @var string
     */
    protected $errorMessage;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Techies\Seatnote\Api\NoteRepositoryInterface $noteRepository
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory $collectionFactory
     * @param string $successMessage
     * @param string $errorMessage
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Techies\Seatnote\Api\NoteRepositoryInterface $noteRepository,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Techies\Seatnote\Model\ResourceModel\Note\CollectionFactory $collectionFactory,
        $successMessage,
        $errorMessage
    ) {
        $this->noteRepository    = $noteRepository;
        $this->filter            = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->successMessage    = $successMessage;
        $this->errorMessage      = $errorMessage;
        parent::__construct($context);
    }

    /**
     * @param \Techies\Seatnote\Api\Data\NoteInterface $note
     * @return mixed
     */
    abstract protected function massAction(\Techies\Seatnote\Api\Data\NoteInterface $note);

    /**
     * execute action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $note) {
                $this->massAction($note);
            }
            $this->messageManager->addSuccessMessage(__($this->successMessage, $collectionSize));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $this->errorMessage);
        }
        $redirectResult = $this->resultRedirectFactory->create();
        $redirectResult->setPath('techies_seatnote/*/index');
        return $redirectResult;
    }
}
