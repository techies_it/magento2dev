<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Controller\Adminhtml\Note;

class Save extends \Techies\Seatnote\Controller\Adminhtml\Note
{
    /**
     * Note factory
     * 
     * @var \Techies\Seatnote\Api\Data\NoteInterfaceFactory
     */
    protected $noteFactory;

    /**
     * Data Object Processor
     * 
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Data Object Helper
     * 
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Uploader pool
     * 
     * @var \Techies\Seatnote\Model\UploaderPool
     */
    protected $uploaderPool;

    /**
     * Data Persistor
     * 
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Techies\Seatnote\Api\NoteRepositoryInterface $noteRepository
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Techies\Seatnote\Api\Data\NoteInterfaceFactory $noteFactory
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Techies\Seatnote\Model\UploaderPool $uploaderPool
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Techies\Seatnote\Api\NoteRepositoryInterface $noteRepository,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Techies\Seatnote\Api\Data\NoteInterfaceFactory $noteFactory,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
      //  \Techies\Seatnote\Model\UploaderPool $uploaderPool,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->noteFactory         = $noteFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper    = $dataObjectHelper;
   //     $this->uploaderPool        = $uploaderPool;
        $this->dataPersistor       = $dataPersistor;
        parent::__construct($context, $coreRegistry, $noteRepository, $resultPageFactory);
    }

    /**
     * run the action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Techies\Seatnote\Api\Data\NoteInterface $note */
        $note = null;
        $postData = $this->getRequest()->getPostValue();
        $data = $postData;
        $id = !empty($data['note_id']) ? $data['note_id'] : null;
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id) {
                $note = $this->noteRepository->getById((int)$id);
            } else {
                unset($data['note_id']);
                $note = $this->noteFactory->create();
            }
            $this->dataObjectHelper->populateWithArray($note, $data, \Techies\Seatnote\Api\Data\NoteInterface::class);
            $this->noteRepository->save($note);
            $this->messageManager->addSuccessMessage(__('You saved the Note'));
            $this->dataPersistor->clear('techies_seatnote_note');
            if ($this->getRequest()->getParam('back')) {
                $resultRedirect->setPath('techies_seatnote/note/edit', ['note_id' => $note->getId()]);
            } else {
                $resultRedirect->setPath('techies_seatnote/note');
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->dataPersistor->set('techies_seatnote_note', $postData);
            $resultRedirect->setPath('techies_seatnote/note/edit', ['note_id' => $id]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('There was a problem saving the Note'));
            $this->dataPersistor->set('techies_seatnote_note', $postData);
            $resultRedirect->setPath('techies_seatnote/note/edit', ['note_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @param string $type
     * @return \Techies\Seatnote\Model\Uploader
     * @throws \Exception
     */
   /* protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }*/
}
