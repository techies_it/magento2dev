<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Controller\Adminhtml\Note;

class Edit extends \Techies\Seatnote\Controller\Adminhtml\Note
{
    /**
     * Initialize current Note and set it in the registry.
     *
     * @return int
     */
    protected function initNote()
    {
        $noteId = $this->getRequest()->getParam('note_id');
        $this->coreRegistry->register(\Techies\Seatnote\Controller\RegistryConstants::CURRENT_NOTE_ID, $noteId);

        return $noteId;
    }

    /**
     * Edit or create Note
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $noteId = $this->initNote();

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Techies_Seatnote::seatnote_note');
        $resultPage->getConfig()->getTitle()->prepend(__('Notes'));
        $resultPage->addBreadcrumb(__('Seat Notes'), __('Seat Notes'));
        $resultPage->addBreadcrumb(__('Notes'), __('Notes'), $this->getUrl('techies_seatnote/note'));

        if ($noteId === null) {
            $resultPage->addBreadcrumb(__('New Note'), __('New Note'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Note'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Note'), __('Edit Note'));
            $resultPage->getConfig()->getTitle()->prepend(
                $this->noteRepository->getById($noteId)->getNote()
            );
        }
        return $resultPage;
    }
}
