<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Api\Data;

/**
 * @api
 */
interface NoteSearchResultInterface
{
    /**
     * Get Notes list.
     *
     * @return \Techies\Seatnote\Api\Data\NoteInterface[]
     */
    public function getItems();

    /**
     * Set Notes list.
     *
     * @param \Techies\Seatnote\Api\Data\NoteInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
