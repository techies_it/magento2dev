<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Api\Data;

/**
 * @api
 */
interface NoteInterface
{
    /**
     * ID
     * 
     * @var string
     */
    const NOTE_ID = 'note_id';

    /**
     * Note attribute constant
     * 
     * @var string
     */
    const NOTE = 'note';

    /**
     * Description attribute constant
     * 
     * @var string
     */
    const DESCRIPTION = 'description';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getNoteId();

    /**
     * Set ID
     *
     * @param int $noteId
     * @return NoteInterface
     */
    public function setNoteId($noteId);

    /**
     * Get Note
     *
     * @return mixed
     */
    public function getNote();

    /**
     * Set Note
     *
     * @param mixed $note
     * @return NoteInterface
     */
    public function setNote($note);

    /**
     * Get Description
     *
     * @return mixed
     */
    public function getDescription();

    /**
     * Set Description
     *
     * @param mixed $description
     * @return NoteInterface
     */
    public function setDescription($description);
}
