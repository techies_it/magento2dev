<?php
/**
 * Techies_Seatnote extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Seatnote
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Techies\Seatnote\Api;

/**
 * @api
 */
interface NoteRepositoryInterface
{
    /**
     * Save Note.
     *
     * @param \Techies\Seatnote\Api\Data\NoteInterface $note
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Techies\Seatnote\Api\Data\NoteInterface $note);

    /**
     * Retrieve Note
     *
     * @param int $noteId
     * @return \Techies\Seatnote\Api\Data\NoteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($noteId);

    /**
     * Retrieve Notes matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Techies\Seatnote\Api\Data\NoteSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Note.
     *
     * @param \Techies\Seatnote\Api\Data\NoteInterface $note
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Techies\Seatnote\Api\Data\NoteInterface $note);

    /**
     * Delete Note by ID.
     *
     * @param int $noteId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($noteId);
}
