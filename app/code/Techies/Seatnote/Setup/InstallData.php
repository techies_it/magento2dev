<?php
namespace Techies\Seatnote\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $notes = array(
            array('S1S','ARMREST/FOLDING SEAT BACKS COVERED (UNUSABLE)'),
            array('S2S','HEADREST COVERED BY SEAT COVER (UNUSABLE)'),
            array('S6S	','SEATS HAVE INTEGRATED SEAT BELTS'),
            array('S7S','Seat Belt Hardware'),
            array('S9S','HEADRESTS ARE NOT COVERED'),
            array('S10S	','HEADREST COVERS INCLUDED'),
            array('S11S','ARMREST COVER INCLUDED'),
            array('S12S','CONSOLE COVER INCLUDED'),
            array('S13S','Safe to use with Seat Air Bags'),
            array('S14S','Manual Seats'),
            array('S15S','ELECTRIC SEATS'),
            array('S16S','CUT OUT FOR HANDLE ON BACK OF SEAT'),
            array('S17S','ARMRESTS ARE NOT COVERED'),
            array('S19S','LEATHER SEAT ONLY'),
            array('S20S','CENTER BACKREST IS STATIONARY'),
            array('S21S','CONSOLE NOT COVERED'),
            array('S23S','2 LUMBAR CONTROLS ON THE DRIVERS SIDE'),
            array('S24S','KNOB ON OUTSIDE DRIVERS SEAT'),
            array('S25S','LEVER ON INSIDE DRIVERS SEAT'),
            array('S27S','SHOULDER SEAT BELT'),
            array('S28S','CUP HOLDER ON BOTTOM CUSHION'),
            array('S29S','BOTTOM CUSHION FLIPS UP'),
            array('S30S','ARMREST HAS CUP HOLDER'),
            array('S32S','WITH GEAR INDENT'),
            array('S33S','WITH LUMBAR'),
            array('S34S','CENTER SEAT OPENS FOR STORAGE'),
            array('S35S','ELECTRIC DRIVERS SEAT ONLY'),
            array('S36S','Restricts use of Child Safety Seat'),
            array('S40S','LARGE HEADRESTS'),
            array('S41S','CUT OUT FOR LATCH ON BACKREST DRIVERS SIDE'),
            array('S44S','CUTOUT FOR LATCHES IN BACK OF SEAT'),
            array('S45S','HIDEAWAY BENCH / FOLDS INTO FLOOR'),
            array('S79S',"LEVER ON CUSHION OF DRIVER'S SEAT"),
            array('S80S',"Lever on outside backrest of Driver's Seat"),
            array('S81S','Console HAS CUP HOLDER'),
            array('S82S','PASSENGER SEAT FOLDS FORWARD FLAT'),
            array('S83S',"CUTOUT FOR LATCHES ON OUTSIDE OF DRIVER'S SEAT"),
            array('S84S','BACKREST FOLDS DOWN'),
            array('S85S','CUTOUT FOR LATCHES ON OUTSIDE TOP'),
            array('S86S','CUTOUT FOR LATCHES ON OUTSIDE OF PASSENGER SEAT'),
            array('S87S','Lever on outside backrest of Passenger Seat'),
            array('S88S','Molded Headrest'),
            array('S89S','Solid Cushion'),
            array('S90S','Seat has Bolsters'),
            array('S91S','RECESSED ARMREST'),
            array('S92S','LEVER ON CUSHION OF PASSENGER SEAT'),
            array('S93S','LEVER ON OUTSIDE SEAT CUSHION'),
            array('S94S','NON REMOVABLE HEADRESTS'),
            array('S95S','2ND ROW FOLDS FORWARD AGAINST THE FRONT SEATS'),
            array('S97S','CHILD RESTRAINTS CUT'),
            array('S100S','HANDLE ON OUTSIDE DRIVERS SEAT'),
            array('S101S','DO NOT COVER SEATS WITH SEAT AIRBAGS'),
            array('S102S','Cloth Seats Only'),
            array('S105S','Solid Backrest'),
            array('S107S','VINYL SEATS ONLY'),
            array('S5AS','ONE SIDE SEAT IMPACT AIRBAG'),
            array('S70S','Driver Lumbar Control'),
            array('S71S','Passenger Lumbar Control '),
            array('S72S','Console has 3 Cupholders'),
            array('S73S','Console has NO Cupholders'),
            array('S74S','Without third row seat'),
            array('S75S','Passenger side inside cupholder'),
            array('S76S','Drivers side inside armrest'),
            array('S77S','Console Lid Cover Included'),
            array('S78S','Rear Pocket Cut Out'),
            array('S108S','Cut out for straps on seat back'),
            array('S109S','USB Port cut outs on back of seat'),
            array('S110S','Pull straps on outside of seats'),
            array('S111S','2 Door'),
            array('S112S','4 Door'),
            array('S113S','Rear Map Pockets'),
        );
        foreach ($notes as $key => $value) {
            $setup->getConnection()->insert(
                $setup->getTable('techies_seatnote_note'),
                [
                    'note' => $value[0],
                    'description' => $value[1]
                ]
            );
        }
        $setup->endSetup();
    }
}