<?php

namespace Techies\Cardetail\Model\Config;

class Multiselect extends \Magento\Captcha\Model\Config\Form\AbstractForm {

    /**
     * Returns options for form multiselect
     *
     * @return array
     */
    public function toOptionArray() {
        $startdate = 1985;
        $enddate = date("Y") + 5;
        $data = array();
        $years = array_reverse(range($startdate, $enddate));
        foreach ($years as $year) {
            $data[] = ['value' => $year, 'label' => "$year"];
        }
        return $data;
    }

}
