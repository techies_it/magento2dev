<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Cardetail\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $carmodelbock;
    protected $_smartsearchdata;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Techies\Carmodel\Block\Carmodel\CarModel $carmodelbock, \Techies\SmartSearch\Block\SmartSearch $smartsearchdata
    ) {
        $this->_pageFactory = $pageFactory;
        $this->carmodelbock = $carmodelbock;
        $this->_smartsearchdata = $smartsearchdata;
        return parent::__construct($context);
    }

    public function execute() {

        $car_year = $this->getRequest()->getParam('car_year'); //1996
        $car_make = $this->getRequest()->getParam('car_make'); //6;
        $car_model = $this->getRequest()->getParam('car_model'); //610; 
        $productid = $this->getRequest()->getParam('productid'); //4;
        $data = $this->carmodelbock->getCarDataByYearMakeModel($car_year, $car_make, $car_model);
        if (count($data) > 0) {
            $session = $this->_smartsearchdata->getCatalogSessionData();
            $session->setCarYear($car_year);
            $session->setCarMake($car_make);
            $session->setCarModel($car_model);
            if (!empty($productid)) {
                $dataCar = $this->carmodelbock->getProductsByIdandModel($productid, $data);
                /* set products */
                $resultPage = $this->_pageFactory->create();
                $block = $resultPage->getLayout()
                        ->createBlock('Magento\Framework\View\Element\Template')
                        ->setTemplate('Techies_Cardetail::product.phtml')
                        ->setDataCar($dataCar)
                        ->toHtml();
                echo $block;
            }
        }
        die();
    }

}
