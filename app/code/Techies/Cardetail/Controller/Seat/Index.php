<?php

/**
 * Techies_Carmodel extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carmodel
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Cardetail\Controller\Seat;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_pageFactory;
    protected $carmodelbock;

    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Techies\Carmodel\Block\Carmodel\CarModel $carmodelbock
    ) {
        $this->_pageFactory = $pageFactory;
        $this->carmodelbock = $carmodelbock;

        return parent::__construct($context);
    }

    public function execute() {
        $car_year = $this->getRequest()->getParam('car_year'); //2000
        $car_make = $this->getRequest()->getParam('car_make'); //6;
        $car_model = $this->getRequest()->getParam('car_model'); //84; 
        $seatid = $this->getRequest()->getParam('seatid'); //11A
        $localtion = $this->getRequest()->getParam('localtion'); //FRONT
        $data = $this->carmodelbock->getSeatNoteByStyleId($car_year, $car_make, $car_model, $seatid, $localtion);
        if (count($data) > 0) {
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
        die();
    }

}
