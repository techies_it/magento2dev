<?php

/**
 * Techies_Carbrand extension
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category  Techies
 * @package   Techies_Carbrand
 * @copyright Copyright (c) 2018
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Techies\Cardetail\Block;

use Magento\Catalog\Model\ProductRepository;

class CategoryProduct extends \Magento\Framework\View\Element\Template {

    protected $categoryFactory;
    protected $_productloader;
    protected $_filesystemdata;
    protected $current_categorydata;
    protected $productRepository;
    protected $scopeConfig;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Catalog\Model\CategoryFactory $categoryFactory, \Magento\Catalog\Model\ProductRepository $_productloader, \Magento\Store\Model\StoreManagerInterface $filesystemdata, \Magento\Framework\Registry $current_categorydata, ProductRepository $productRepository, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, array $data = []
    ) {
        $this->productRepository = $productRepository;
        $this->current_categorydata = $current_categorydata;
        $this->_productloader = $_productloader;
        $this->categoryFactory = $categoryFactory;
        $this->_filesystemdata = $filesystemdata;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * fetch data by current cat id
     * @return mixed
     */
    public function getCategory() {
        $category = $this->current_categorydata->registry('current_category'); //get current category
        $categoryId = $category->getId();
        $category = $this->categoryFactory->create()->load($categoryId);
        return $category;
    }

    /**
     * get product data by prodcut id
     * @param $id
     * @return mixed
     */
    public function getLoadProduct($id) {
        return $this->_productloader->getById($id);
    }

    /**
     * fetch product collection
     * @return mixed
     */
    public function getProductCollection() {
        return $this->getCategory()->getProductCollection()->addAttributeToSelect('*');
    }

    /**
     * fetch productRepository by product id
     * @param $id
     * @return mixed
     */
    public function getProductUrl($id) {
        return $product = $this->productRepository->getById($id);
    }

    /**
     * get media file path
     * @return mixed
     */
    public function getFilePath() {

        return $this->_filesystemdata->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * return years from config setting
     * @return array
     */
    public function getYearsFromCoreConfig() {
        $data = array();
        $startdate = 1985;
        $enddate = date("Y") + 1;
        $years = $this->scopeConfig->getValue("cardetail_entry_1/general/cardetail_years");
        $years = explode(",", $years);
        if (empty($years)) {
            $years = array_reverse(range($startdate, $enddate));
        }
        foreach ($years as $year) {
            if (!empty($year)) {
                $data[] = ['value' => $year, 'label' => "$year"];
            }
        }
        return $data;
    }

}
