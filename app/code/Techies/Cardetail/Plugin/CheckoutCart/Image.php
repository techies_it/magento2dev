<?php

namespace Techies\Cardetail\Plugin\CheckoutCart;

class Image {

    protected $_productloader;
    protected $productparent;

    public function __construct(
    \Techies\Cardetail\Block\CategoryProduct $_productloader, \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $productparent, array $data = []
    ) {
        $this->_productloader = $_productloader;
        $this->productparent = $productparent;
    }

    /**
     * set image of product in cart
     * @param $item
     * @param $result
     * @return mixed
     */
    public function afterGetImage($item, $result) {
        $img = '';

        $options = $item->getProductOptions();
        if (count($options) > 0) {
            foreach ($options as $option) {
                $itemOptions = $option['value'];
                if ($option['label'] == "Color") {
                    $option['product_id'] = $result->getProductId();

                    $configProduct = $this->_productloader->getLoadProduct($option['product_id']);
                    $product = $this->productparent->getParentIdsByChild($configProduct->getId());
                    if (isset($product[0])) {
                        $parentId = $this->_productloader->getLoadProduct($product[0]);

                        $images = $parentId->getMediaGalleryEntries();
                        if (count($images) > 0) {
                            foreach ($images as $k => $valImages) {
                                /// echo $option['value'];
                                $dataImg = $valImages->getData();
                                $colorProduct = str_replace(array(" ", '(', ')'), '', $dataImg['label']);
                                $colorProductOption = str_replace(array(" ", '(', ')'), '', $option['value']);
                                $colorNameData = substr($colorProductOption, 0, -2);
                                if (strtolower($colorProduct) == strtolower($colorNameData)) {
                                    $media = $this->_productloader->getFilePath();
                                    $img = $media . "catalog/product" . $valImages['file'];
                                    $result->setImageUrl($img);
                                }
                            }
                        }
                    }
                }
            }
            if (!empty($img)) {
                $result->setImageUrl($img);
            }
            return $result;
        }
    }

}
